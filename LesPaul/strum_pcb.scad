include <./commonVars.scad>

thichness = pcbThickness;

module PCBHoles(x)
{
    hole1D = 4.5;
    hole2D = 2.5;
    holeFn = 32;

    translate([x, 0, -5])
    {
        cylinder(d=hole1D, h=10, $fn=holeFn);

        translate([0, 32/2, 0])
        cylinder(d=hole2D, h=10, $fn=holeFn);
        translate([0, -32/2, 0])
        cylinder(d=hole2D, h=10, $fn=holeFn);
    }
}

module StrumPCB()
{
    width = PCBWidth;
    lenght = PCBLenght;

    color("#00A000")
    difference()
    {

        translate([0, (lenght/2 - 26), 0])
        cube([width, lenght, thichness], center=true);

        PCBHoles(PCBOffetX);
        PCBHoles(-PCBOffetX);
    }
}

StrumPCB();