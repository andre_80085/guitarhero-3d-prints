use <NopSCADlib/utils/quadrant.scad>

module SwitchMount(width = 12, d = 4, cutout = false)
{
    w = (width + d + 1) * 2;
    l = 10;
    h = d + 10;
    holeDepth = 7.5;
    d_marging = 0.1;

    yOffset = l/2;
    xOffset = width;

    pcb_cutoutY = 1.65;
    pcbW = 19.95;
    pcbW2 = 19.95;
    yOffset3 = 2;
    l2 = 6;
    h2 = 15;

    fn1 = $preview ? 50 : 100;

    h3 = 4;

    color("#BF406D")
    difference()
    {
        union()
        {
            translate([0,0,0.75])
            cube([w, l, h], center=true);

            //Side mounts
            RodEndMount(w);
            mirror([1,0,0])
            RodEndMount(w);
        }
        

        //Switch cutout
        translate([0,0,-1])
        cube([16.5, l + 0.1, h2], center=true);

        //Switch PCB cutout
        translate([0,yOffset3 + 0.75, -1])
        cube([pcbW2, pcb_cutoutY, pcbW2 - 1], center=true);

        //Rod hole 1
        translate([width, -yOffset, 0.25])
        rotate([90,0,0])
        cylinder(d=d+0.05, h=(holeDepth * 2) + 0.25, center=true, $fn=fn1);

        translate([width, 0, 0.25])
        rotate([90,0,0])
        cylinder(d=2, h=20, center=true, $fn=fn1);

        //Rod hole 2
        mirror([1, 0, 0])
        translate([width, -yOffset, 0.25])
        rotate([90,0,0])
        cylinder(d=d+0.05, h=(holeDepth * 2 + 0.25), center=true, $fn=fn1);

        mirror([1, 0, 0])
        translate([width, 0, 0.25])
        rotate([90,0,0])
        cylinder(d=2, h=20, center=true, $fn=fn1);


        translate([-20,0,-10])
        rotate([0,45,0])
        cube([20, 15, 10], center=true);
        
        mirror([1,0,0])
        translate([-20,0,-10])
        rotate([0,45,0])
        cube([20, 15, 10], center=true);

        if(cutout)
        {
            translate([24,0,0])
            cube([20,15,15], center=true);

            translate([11,0,-10])
            cube([10,15,7], center=true);
        }
    }
}

module RodEndMount(w)
{
    xOffset4 = 6.5;
    sideD = 10;

    translate([(w/2) - 1, 5, 3.75])
    difference()
    {
        linear_extrude(height = 4, center = true, convexity = 10, twist = 0)
        difference()
        {
            rotate([0,0,-90])
            quadrant(sideD, 2, center=false);

            translate([6.4,-3.9, 0])
            circle(d=3.1, $fn=48);
        }
    }
}

SwitchMount(14, 6);