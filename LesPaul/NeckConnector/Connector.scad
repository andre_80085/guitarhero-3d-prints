include <Round-Anything/polyround.scad>
use <NopSCADlib/utils/round.scad>

module DB9_Connector(w1, w2, h, l)
{
    
    x1 = w1/2;
    x2 = w2/2;

    radiiPoints=[
        [-x1,h,1],[x1,h,1],
        [x2,0,1],[-x2,0,1]
    ];

    linear_extrude(height = l, center = true, convexity = 10, twist = 0)
    polygon(
        polyRound(radiiPoints,30)
    );
}

module DB9_Reference(male = true)
{
    
    w1 = 16.25;
    w2 = 14;
    h = 8;

    color("#2E7D32")
    union()
    {
        linear_extrude(height = 0.8, center = true, convexity = 10, twist = 0)
        {
            difference()
            {
                round(0.5)
                square([31, 12.5]);
            
                translate([31/2 + 25/2, 12.5/2])
                circle(d=3, $fn=30);
                
                translate([31/2 - 25/2, 12.5/2])
                circle(d=3, $fn=30);
            }
        }

        translate([31/2, 12.5/2, 0])
        if(male)
        {
            translate([0, -4, 5.5/2 + 0.4])
            DB9_Connector(16.25, 14, 8, 5.5);

            translate([0, -5.5, -4/2 - 0.4])
            DB9_Connector(19, 17, 11, 4);
        }
        else
        {
            difference()
            {
                translate([0, -9.5/2, 6/2 + 0.4])
                DB9_Connector(18, 16, 9.5, 6);

                translate([0, -4, 6.4/2 + 0.4])
                DB9_Connector(16.25, 14, 8, 6.5);
            }
            

            translate([0, -5.5, -4/2 - 0.4])
            DB9_Connector(19, 17, 11, 4); 
        }

    }
    

    
}

module BodyConnectorPart()
{
    difference()
    {
        union()
        {
            translate([25/2, -0.25, 0])
            cube([5,5.5,5], center=true);

            translate(
                [21/2 + 2.45/2,
                -1,
                3.5 + 3])
            cube([2.45, 4, 9], center=true);

            translate(
                [20/2 + 3/2,
                -1,
                -3.5 - 3])
            cube([3, 4, 9], center=true);
        }

        #translate([25/2, 1, 0])
        rotate([-90,0,0])
        cylinder(d=2.85, h=3.2, center=true, $fn=50);
    }
}

module BodyConnector()
{
    BodyConnectorPart();
    mirror([1, 0, 0])
    BodyConnectorPart();
}

module PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l)
{
    cylinder(d=dHole_conn, h=10, center=true, $fn=50);
            
    translate([0, 0, pin_conn_l/2 + 0.01])
    cylinder(d=dHole_bump, h=pin_bump_l, center=true, $fn=50);

    translate([0,0, -pin_conn_l/2 + 0.749])
    rotate([180,0,0])
    linear_extrude(height = 3, center = true, convexity = 10, scale=1.25)
    circle(d=dHole_conn, $fn=100);
}

module NeckConnection()
{
    h1 = 8;     //Backside mold height total
    h2 = 15.2;  //Frontside mold height total
    h3 = 3;     //Backside mold height lower
    h4 = 11;    //Frontside mold height lower
    d = 3.25;   //Space between neck mold and original pcb mount
    t = 1.5;    //Neck mold thickness

    //15 & 13.5, 6.75
    //D 1.45, 2.0 

    pcb_h1 = 19 - 8;
    pcb_h2 = 8;
    pcb_w1 = 20.5;
    pcb_w2 = 26.5;

    //Original PCB
    /*
    translate([0,0,-0.5])
    color("#388E3C")
    linear_extrude(height = 1, center = true, convexity = 10, twist = 0)
    {
        union()
        {
            round(1)
            square([pcb_w2, pcb_h2], center=true);

            translate([0,4 + pcb_h1/2, 0])
            square([pcb_w1, pcb_h1], center=true);
        }
    }
    */

    translate([0, -6.15/2, 8.3 / 2 + 1 + 3.25 + 1.5 - 1])
    difference()
    {
        pin_conn_l = 5;
        pin_bump_l = 1.5;
        pin_crimp_l = 7;

        //DB9_Connector(14.5, 12.5, 6.15, 8.25);
        DB9_Connector(14.5, 12.5, 6.15, pin_conn_l + pin_bump_l);

        dHole_conn = 1.65;
        dHole_bump = 1.85;
        
        //Bottom row
        y1 = 6.15/2 - 1.5;
        x = 10/7.1;

        translate([-3*x, y1, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);

        translate([-x, y1, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);

        translate([x, y1, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);

        translate([3*x, y1, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);

        //Top row
        y2 = 6.15/2 + 1.5;
        x2 = 12.5/9;

        translate([-4*x2, y2, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);

        translate([-2*x2, y2, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);

        translate([0, y2, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);

        translate([2*x2, y2, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);

        translate([4*x2, y2, 0])
        PinCutout(dHole_conn, dHole_bump, pin_conn_l, pin_bump_l);
    }
    
    /*
    translate([0,0, d/2])
    linear_extrude(height = d, center = true, convexity = 10, twist = 0)
    {
        union()
        {
            round(1)
            square([pcb_w2, pcb_h2], center=true);

            translate([0,4 + pcb_h1/2, 0])
            square([pcb_w1, pcb_h1], center=true);
        }
    }

    difference()
    {
        union()
        {
            translate([8,0,5])
            cube([4, 6, 4], center=true);
        }

        translate([0, -6.15/2, 8.3 / 2 + 1 + 3.25 + 1.5 - 1])
        rotate([0,0,0])
        DB9_Connector(18, 16, 6.15, 8.3);
    }
    */
}

/*
translate([-31/2,2.4,0])
rotate([90,0,0])
DB9_Reference(false);
*/

//translate([25/2, -0.5, 12.5/2])
/*
translate([0, -0.5, 12.5/2])
BodyConnector();
*/
NeckConnection();

/*
translate([0,0,14])
cube([13.5, 0.1, 0.1], center=true);
*/
//cube([20, 4, 10], center=true);