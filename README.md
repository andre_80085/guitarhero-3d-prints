# GuitarHero 3D-Prints

A collection of OpenSCAD designs for Guitar Hero controllers.
Both original and custom parts.

[Link to Wiki index for all parts](https://gitlab.com/andre_80085/guitarhero-3d-prints/-/wikis/home)

## Description
This project is a collection of parts and designs for Guitar Hero controllers.
There will be one folder for each major type of controller and subfolders for each part/design including any custom designs.
Parts shared by many controllers are listed under **Common** folder.

Each design intended for printing or usage as reference have a corresponding page on the wiki of this project. 
- Description of the design
- Reference images
- Controller compatability
- Status such as draft, work-in-progress, unverified and verified (printed)
- Link to Thingiverse STL

## Thingiverse
Link to my designs on thingiverse ready to print [https://www.thingiverse.com/andre80085/designs](https://www.thingiverse.com/andre80085/designs)

## Installation
Requires [OpenSCAD](https://openscad.org/)  
Designs in this project utilizes [CadHub RoundAnything](https://learn.cadhub.xyz/docs/round-anything/overview) and therefore must be installed.

## Contributing
Feel free to print and validate any model in this project.
Merge requests must include clear evidence or documentation such as images of print result of both revisions.
If an model is difficult to print on a certain type of printer then a specialized model should be created.

### Disclaimer
Designs in this project are based on the controllers I have access to and can measure.
