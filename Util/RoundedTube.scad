use <NopSCADlib/core.scad>
use <NopSCADlib/utils/quadrant.scad>

//A simple hollowed cylinder with rounded inner corners
module RoundedTube(h = 5, d=4, r = 0.5, fn1=64, fn2=32)
{
    z = (d/2) + r;
    d2 = d + (r*2);

    h2 = h - (2*r);

    union()
    {
        difference()
        {
            cylinder(h=h2, d=d2, center=true, $fn=fn1);
            cylinder(h=h2*2, d=d, center=true, $fn=fn1);
        }

        translate([0,0,h2/2])
        rotate_extrude(convexity = 10, $fn = fn1)
        translate([-z,0,0])
        quadrant(w=r, r=r, center = false, $fn=fn2);

        translate([0,0,-h2/2])
        rotate([180,0,0])
        rotate_extrude(convexity = 10, $fn = fn1)
        translate([-z,0,0])
        quadrant(w=r, r=r, center = false, $fn=fn2);
    }
}

RoundedTube();