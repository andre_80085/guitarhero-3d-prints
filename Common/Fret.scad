include <Round-Anything/polyround.scad>

//Resin printers
UseVacumRelease = false;

//Body
BodyWidth1 = 18;
BodyWidth2 = 17.4;
BodyWidth3 = 17.5;
BodyLength = 27;
BodyHeight = 15;

//SideRotation
SideRotation = 1.5;

//Alignment pins
pinSize = 1.25;
pinW = 1.8;

//Cylinders located directly on top of circuit-closers
//Not used in this design, only for reference
cylinderD = 2;
cylinderX1 = BodyWidth1/2;
cylinderY1 = 10;
cylinderY2 = BodyLength - 10;

//Bump
useBump = true;
bumpLength = 13;
bumpDepth = -0.1;
bumpOffsetZ = 0.35;
bumpOffsetY = -0.0;
bumpX1 = 0.5;
bumpY1 = 0.35;
bumpR = 0.35;

//$fn
bumpFn = $preview ? 8 : 16;
pinFn = $preview ? 8 : 16;
bodyFn = $preview ? 8 : 64;
bodyRotateFn = $preview ? 50 : 200;
cylinderFn = $preview ? 8 : 16;

module FretBase(cornerR, topR, height, w, l)
{
    points = [
        [0, 0, cornerR],
        [w, 0, cornerR],
        [w, l, w/2],
        [0, l, w/2],
    ];

    polyRoundExtrude(points, height, r1=topR, r2=0, fn=bodyFn);
}

module FretBaseOuter(w, w2, w3, height, length)
{
    difference()
    {
        h = height;
        l = length - (w/2);

        union()
        {
            points = [
                [-w/2,-1,0],
                [w/2,-1,0],

                [w2/2, h/3, h/2],

                [w3/2, h - 0.75, 1],
                [0 + w3/4, h, w3*3],

                [0 - w3/4, h, w3*3],
                [-w3/2, h - 0.75, 1],

                [-w2/2, h/3, h/2],
            ];

            difference()
            {
                translate([0, l, 0])
                rotate([90, 0 ,0])
                polyRoundExtrude(points, l, r1=1, r2=0, fn=bodyFn);
            }

            points2 = [
                [0,     -1,          0],
                [w/2,   -1,          0],
                [w2/2,  h/3,        h/2],
                [w3/2,  h - 0.75,      1],
                [w3/4,   h,         w3*3],
                [0,     h,          0],
                [0,     h,          0]
            ];

            polygonArray = polyRound(points2,fn=bodyFn);

            translate([0, l, 0])
            rotate_extrude(angle=180, convexity = 10, $fn = bodyRotateFn)
            polygon(polygonArray);

        }

        translate([0,length/2,-1])
        cube([w + 1, length + 1, 2], center=true);
    }
}

module SidePart()
{
    height = 4;
    yOffset = 3.5;
    length = 14.75;
    rubberSlotLength = 9.25;
    width = 2 + 0.15;
    rubberSlotDepth = 2.5;
    rubberSlotWidth = 1.15;

    color("yellow")
    translate([-width, yOffset, 0])
    {
        rotate([0, SideRotation, 0])
        union()
        {
            difference()
            {
                cube([width, length, height]);

                translate([0.95, (length - rubberSlotLength)/2, height - rubberSlotDepth])
                cube([rubberSlotWidth, rubberSlotLength, rubberSlotDepth + 0.1]);
            }
            
            translate([0,pinSize,0])
            rotate([0,0,-90])
            AlignmentPin(9, 0, 0);

            translate([0,length,0])
            rotate([0,0,-90])
            AlignmentPin(9, 0, 0);
        }
    }
}

module AlignmentPin(h = 8.5, roundBottom = 0.5, rotation = SideRotation)
{
    //h = 8.75;
    w = pinW + 1;

    points = [
        [0, 0, 0],
        [w, 0, roundBottom],
        [w, h, 0.5],
        [0, h, 0]
    ];

    color("orange")
    rotate([-rotation, 0, 0])
    translate([pinSize,w,0])
    rotate([90,0,-90])
    linear_extrude(height = pinSize, center = false, convexity = 10, twist = 0)
    polygon(
        polyRound(points,pinFn)
    );
    
}

module NegativeBump()
{
    y1 = bumpY1;
    x1 = bumpX1;

    points = [
        [-1, 3, 0],
        [-1, 0.4, 0],
        [-x1, y1, 0.3],
        [0, bumpDepth, 0.3],
        [x1, y1, 0.3],
        [1, 0.4, 0],
        [1, 3, 0]
    ];

    polyRoundExtrude(points, bumpLength, r1=bumpR, r2=bumpR, fn=bumpFn);
}

module SlantedCutout(w, l1, h)
{
    points=[ 
        [w,l1,0],   //0: +X +Y
        [w,-l1,0],  //1: +X -Y
        [-w,-l1,0], //2: -X -Y
        [-w,l1,0],  //3: -X +Y
        [-w + l1, 0, l1 * 1.1],   //4 -X
        [w - l1, 0, l1 * 1.1]     //5 +X
        ];
    faces=[ [0,3,2,1],
            [0, 5, 4, 3],
            [2, 4, 5, 1],
            [1, 5, 0],
            [3, 4, 2]
        ];
    
    translate([0, 0, h/2])
    {
        translate([0,0, h/2])
        polyhedron(points, faces);

        
        cube([w*2, l1*2, h + 0.001], center=true);
    }
}

module CutoutPart1(width, length, height = 10)
{
    w = width/2;
    l1 = length/2;
    h = height - (l1 * 1.1);

    SlantedCutout(w, l1, h);
}

module BodyCutoutHoles(h)
{
    translate([5, 19/2 + 2, 0])
    rotate([0,0, 90])
    CutoutPart1(19, 4, h);

    translate([-5, 19/2 + 2, 0])
    rotate([0,0, 90])
    CutoutPart1(19, 4, h);

    translate([0, (10 - 2 - 2)/2 + 2, 0])
    rotate([0,0, 90])
    CutoutPart1(10 - 2 - 2, 4, h);

    translate([0, (cylinderY1) + 2 + ((cylinderY2 - 2) - (12))/2, 0])
    rotate([0,0, 0])
    CutoutPart1(4.5, (cylinderY2 - 2) - (12), h);

    translate([0, (cylinderY2) + 2 + 2.5/2, 0])
    rotate([0,0, 0])
    CutoutPart1(4.5, 2.5, h);

    translate([0, (cylinderY2) + 2 + 2.5/2 + 2.5/2 + 2, 0])
    rotate([0,0, 0])
    CutoutPart1(8.5, 2.5, h);

    //vacum release for resin printers
    if(UseVacumRelease)
    {
        translate([5, 1, 0.4])
        rotate([0,0, 90])
        CutoutPart1(3, 1, 0);

        translate([-5, 1, 0.4])
        rotate([0,0, 90])
        CutoutPart1(3, 1, 0);

        translate([-2.5, 5, 0.4])
        rotate([0,0, 0])
        CutoutPart1(3, 1, 0);

        translate([2.5, 5, 0.4])
        rotate([0,0, 0])
        CutoutPart1(3, 1, 0);

        translate([-2.5, 13.5, 0.4])
        rotate([0,0, 0])
        CutoutPart1(3, 1, 0);

        translate([2.5, 13.5, 0.4])
        rotate([0,0, 0])
        CutoutPart1(3, 1, 0);

        translate([-2.5, 19.75, 0.4])
        rotate([0,0, 0])
        CutoutPart1(3, 1, 0);

        translate([2.5, 19.75, 0.4])
        rotate([0,0, 0])
        CutoutPart1(3, 1, 0);

        translate([0, 21.5, 0.4])
        rotate([0,0, 90])
        CutoutPart1(3, 1, 0);
    }
}

module OverallSizeReferences()
{
    translate([0, BodyLength/2, 7.5])
    color("Pink")
    cube([21.5, 1, 1], center=true);

    translate([0, BodyLength/2, 0])
    color("Green")
    cube([22, 1, 1], center=true);

    translate([0, BodyLength/2, 0])
    color("Purple")
    cube([1, 30.5, 1], center=true);
}

module FullFret()
{
    difference()
    {
        union()
        {

            FretBaseOuter(BodyWidth1, BodyWidth2, BodyWidth3, BodyHeight, BodyLength);

            translate([-(BodyWidth1/2) + 0.2, 0, 0])
            SidePart();

            translate([(BodyWidth1/2) - 0.2, 0, 0])
            mirror([1, 0, 0])
            SidePart();

            translate([-(pinSize/2), - pinW - 0.2, 0])
            AlignmentPin(8.5, 0, SideRotation);

            translate([(pinSize/2), BodyLength + pinW + 0.2, 0])
            rotate([0, 0, 180])
            AlignmentPin(8.5, 0, SideRotation);

            /*
            //Cylinders for reference
            translate([0, cylinderY1, (BodyHeight - 1)/2])
            cylinder(h=(BodyHeight - 1), d=4, center=true, $fn=cylinderFn);

            translate([0, cylinderY2, (BodyHeight - 1)/2])
            cylinder(h=(BodyHeight - 1), d=4, center=true, $fn=cylinderFn);
            */

        }

        //Remove any parts sticking out below Z 0.0
        translate([0,0,-25])
        cube([100, 100, 50], center=true);

        if(useBump)
        {
            translate([0, BodyLength/2 - (bumpLength/2) + bumpOffsetY, BodyHeight - bumpOffsetZ])
            rotate([90, 0, 180])
            NegativeBump();
        }

        translate([0, 0, -0.01])
        BodyCutoutHoles(BodyHeight - 1);
    }

}

FullFret();

//OverallSizeReferences();