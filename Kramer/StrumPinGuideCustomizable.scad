D1 = 6.5;
D2 = 9;
D3 = 4;

H1 = 3;
H2 = 1.5;

module PinGuide(d1, d2, d3, h1, outerLength, fn = 32)
{
    ht = h1 + outerLength;

    translate([0, 0, ht/2])
    difference()
    {
        union()
        {
            cylinder(h=ht, d=d1, center=true, $fn=fn);
            translate([0, 0, (outerLength/2) + (ht/2) - outerLength])
                cylinder(h=outerLength, d=d2, center=true, $fn=fn);
        }
        cylinder(h=ht*2, d=d3, center=true, $fn=fn);
    }

}

PinGuide(D1, D2, D3, H1, H2, 64);