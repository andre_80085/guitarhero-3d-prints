include <StrumVars.scad>
use <StrumProfile.scad>

bodyOffsetZ = 0;
feetLengthZ = 15;
width2 = 36;
bodyLength = 64;
gap = 23;
centerCutoutH = 9 - (feetHeight + feetOffset);
centerZ = centerCutoutH + (feetHeight + feetOffset);

CenterExtraLength = 13;
CenterCutoutHeight = 4.5;

//fns
centerFn = 32 + 10;
guidepinFn = 64 + 10;
innerWallsFn = 32 + 10;
centerCutoutFn = 32 + 10;

module Extruded(length)
{
    rotate([90,0,0])
        linear_extrude(height = length, center = true, convexity = 10, twist = 0)
            FullProfile();
}

module CenterExtra()
{
    rotate([90, 0, 0])
        cylinder(h=CenterExtraLength, d=1, center=true, $fn=centerFn);
}

module GuidepinCutout()
{
    translate([0, 0, 2.5 + (PinHole/2)])
        rotate([90, 0, 0])
            cylinder(h=bodyLength*2, d=PinHole, center=true, $fn=guidepinFn);
}

module InnerBodyCutout(length)
{
    rotate([90,0,0])
        linear_extrude(height = length, center = true, convexity = 10, twist = 0)
            StrumProfileCutout();
}

module InnerBodyWalls()
{
    points = [
        [-4.5, -1, 0],
        [-4.5, 0, 0],
        [-2, 10, 3],
        //[0, 10.5, 1],
        [2, 10, 3],
        [4.5, 0, 0],
        [4.5, -1, 0]
    ];

    difference()
    {
        resize(newsize=[bodyWidth, 1, 22], auto=false)
        {
            difference()
            {
                Extruded(1);
                cube([1000, 1000, 0], center=true);
            }
        }


        rotate([90,0,00])
            linear_extrude(height = 2, center = true, convexity = 10, twist = 0)
                polygon(polyRound(points,innerWallsFn));
    }
}

module GuidepinOuter(yMod)
{
    y = (bodyLength/2);
    z = 2.5 + (PinHole/2);
    translate([0, y * yMod, z])
        rotate([90,0,0])
            cylinder(h=1.5, d=PinHoleGuideD, center=true, $fn=guidepinFn);
}

module BodyCenterCutout()
{
    cutoutWidth = 20;
    cutoutWidth2 = 22;
    points = [
        [-cutoutWidth2/2, 0, 0],
        [-cutoutWidth2/2, -0.1, 0],
        [cutoutWidth2/2, -0.1, 0],
        [cutoutWidth2/2, 0, 0],
        [cutoutWidth/2, CenterCutoutHeight, 0],
        [-cutoutWidth/2, CenterCutoutHeight, 0],
    ];

    rotate([90,0,90])
    linear_extrude(height = bodyWidth*2, center = true, convexity = 10, twist = 0)
    polygon(polyRound(points,centerCutoutFn));
}

module Feet()
{
    w2 = 6.25;
    w = w2 + bodyThickness;
    h = 3.25;
    l = 14.5;
    translate(
        [(bodyWidth/2) + (w2/2) - bodyThickness/2, 
        (bodyLength/2) - 3.5 - (l/2),
        (0.02 -h/2) ])
    {
        difference()
        {
            cube([w, l, h], center=true);

            translate([(w/2) - (1.1/2) - 2, 0, 0])
            cube([1.1, 10, feetHeight*2], center=true);

            translate([-w/2,0, (feetHeight/-2) - 1])
            rotate([0,-45,0])
            cube([feetHeight,l+2,feetHeight], center=true);
        }
        
    }
}

module StrumBody()
{
    union()
    {
        difference()
        {
            //Main part
            Extruded(bodyLength);

            InnerBodyCutout(bodyLength - (2*ProfileThickness) );
            
            BodyCenterCutout();

            GuidepinCutout();
            GuidepinOuter(1);
            GuidepinOuter(-1);
        }

        //Supporting walls inside
        centerExtraX = bodyWidth/2 - (bodyThickness/2);
        translate([centerExtraX, 0, CenterCutoutHeight])
            CenterExtra();
        translate([-centerExtraX, 0, CenterCutoutHeight])
            CenterExtra();
        
        
        translate([0, gap/2 + 0.5, 0])
            InnerBodyWalls();
        translate([0, (-gap/2 - 0.5), 0])
            InnerBodyWalls();

        //Feets
        Feet();

        mirror([1,0,0])
        Feet();

        mirror([0,-1,0])
        Feet();
        
        mirror([0,-1,0])
        mirror([1,0,0])
        Feet();
    }
}

difference()
{
    StrumBody();
    /*
    translate([0,2,10])
    cube([40,bodyLength - 1.95,50], center=true);
    */
}
