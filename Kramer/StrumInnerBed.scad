include <StrumVars.scad>

width = 11;
height = 6.5;
length = 19.5;
wallThickness = 1;
HoleSpacing = 10;
LowerCylinderH = 9 - height;
HoleR = 2;


module LowerCylinderPart()
{
    difference(){
        cylinder(h=LowerCylinderH, r=3, center=true, $fn=32);
        
        translate([0, 0, -(LowerCylinderH/2)])
        cylinder(h=2, r=HoleR, center=true, $fn=32);
    }
    
}

module LowerPart()
{
    union()
    {
        translate([0, (HoleSpacing/2), 0])
        LowerCylinderPart();

        translate([0, -(HoleSpacing/2), 0])
        LowerCylinderPart();

        translate([0, 0, 0])
        cube([1, HoleSpacing - (2 * HoleR), LowerCylinderH], center=true);
    }
    
}

module CutoutHole(yMod)
{
    y = HoleSpacing / 2;
    translate([0, y * yMod, 0])
    {
        cylinder(h=100, r=1, center=true, $fn=32);

        translate([0,0, -(height/2) + 1])
        cylinder(h=2, r=HoleR, center=true, $fn=32);
    }
    


}

module StrumInnerBed()
{

    difference()
    {
        union()
        {
            difference()
            {
                cube([width, length, height], center=true);
                translate([0,0,wallThickness])
                    cube([width - (2 * wallThickness), length - (2 * wallThickness), height], center=true);
                
                translate([0, 0, height/2])
                rotate([90, 0, 0])
                resize(newsize=[0, width - 4, 0], auto=false)
                cylinder(h=100, d=8, center=true, $fn=23);
            }

            translate([0,0,-(height/2) - LowerCylinderH/2,])
            LowerPart();
        }

        CutoutHole(1);
        CutoutHole(-1);
    }
    

}

StrumInnerBed();