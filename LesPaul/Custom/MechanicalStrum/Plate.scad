include <NopSCADlib/vitamins/nuts.scad>
include <Round-Anything/polyround.scad>
use <NopSCADlib/utils/round.scad>

module SwitchPlate3(spacing = 1)
{
    switchL = 15 * 2 + spacing;
    switchW = 15.6;
    switchMountHole = switchL/2 + 4;

    difference()
    {
        union()
        {
            //base plate
            linear_extrude(height = 2, center = true, convexity = 10, twist = 0)
            round(3)
            square([61,61], center=true);

            //structural addition
            translate([0,0,2.5])
            {
                linear_extrude(height = 3, center = true, convexity = 10, twist = 0)
                difference()
                {
                    round(3)
                    square([61,61], center=true);

                    square([57.5,57.5], center=true);
                }

                MountingCorner();

                mirror([0,1,0])
                MountingCorner();

                mirror([1,0,0])
                MountingCorner();

                mirror([1,0,0])
                mirror([0,1,0])
                MountingCorner();

                
                linear_extrude(height = 3, center = true, convexity = 10, twist = 0)
                {
                    translate([switchW/2 + 1,0,0])
                    square([2, 61], center=true);

                    translate([-switchW/2 - 1,0,0])
                    square([2, 61], center=true);

                    rotate([0,0,45])
                    square([1.5, 70], center=true);

                    rotate([0,0,-45])
                    square([1.5, 70], center=true);
                
                    translate([0,switchMountHole,0])
                    circle(d=6, $fn=100);

                    translate([0,-switchMountHole,0])
                    circle(d=6, $fn=100);
                }
            }

            //Sping mounts
            points3 = [
                [0,     4,  0],
                [2.15,     4,  1],
                [2.3,     2,  0],
                [2.3,   0, 0],
                [0,   0, 0]
            ];

            translate([20,0,1])
            rotate([0,0,0])
            rotate_extrude(convexity = 10, $fn = 180)
            polygon(polyRound(points3,20));

            translate([-20,0,1])
            rotate([0,0,0])
            rotate_extrude(convexity = 10, $fn = 180)
            polygon(polyRound(points3,20));
        }
        

        //Cutout holes
        translate([0,0,1.5])
        linear_extrude(height = 5.05, center = true, convexity = 10, twist = 0)
        {
            square([switchW,switchL], center=true);
            
            //Cutouts for mounting
            Cutouts();

            mirror([0,1,0])
            Cutouts();

            mirror([1,0,0])
            Cutouts();

            mirror([1,0,0])
            mirror([0,1,0])
            Cutouts();

            translate([0,switchMountHole,0])
            circle(d=3.2, $fn=100);
        
            translate([0,-switchMountHole,0])
            circle(d=3.2, $fn=100);
        }   
        
    }
    
}

module SwitchPlate2()
{
    linear_extrude(height = 5, center = true, convexity = 10, twist = 0)
    difference()
    {
        round(4)
        square([61,61], center=true);

        translate([0,12.5,0])
        square([17.5,17.5], center=true);

        translate([0,-12.5,0])
        square([17.5,17.5], center=true);

        translate([8,0,0])
        circle(d=3.1, $fn=48);
    
        translate([-8,0,0])
        circle(d=3.1, $fn=48);

        Cutouts();

        mirror([0,1,0])
        Cutouts();

        mirror([1,0,0])
        Cutouts();

        mirror([1,0,0])
        mirror([0,1,0])
        Cutouts();

        translate([20,0,0])
        square([8, 30], center=true);
    }
    
}


module Cutouts()
{
    translate([26.4, 26.6, 0])
    circle(d=3.5, $fn=100);

    //translate([20, 14, 0])
    //cube([10,12,10], center=true);
}

module MountingCorner()
{
    translate([26.4, 26.6, 0])
    linear_extrude(height = 3, center = true, convexity = 10, twist = 0)
    round(2)
    square([7,7], center=true);
}

module SwitchPlate()
{
    difference()
    {
        union()
        {
            translate([0,0,23])
            cube([61,61, 5], center=true);

            translate([10.55,0,27])
            cube([5, 40, 6], center=true);

            translate([-10.55,0,27])
            cube([5, 40, 6], center=true);
        }

        translate([0,0,5.5])
        rotate([10,0,0])
        translate([0,14.5,18.45])
        cube([20,20, 1.7], center=true);

        translate([0,0,3])
        rotate([10,0,0])
        translate([0,15,15])
        cube([16,16, 20], center=true);

        mirror([0,1,0])
        {
            translate([0,0,5.5])
            rotate([10,0,0])
            translate([0,14.5,18.45])
            cube([20,20, 1.7], center=true);

            translate([0,0,3])
            rotate([10,0,0])
            translate([0,15,15])
            cube([16,16, 20], center=true);
        }

        //spring guide
        mirror([1,0,0])
        {
            translate([20, 0, 20])
            cylinder(h=6, d=6, $fn=50);

            translate([20, 0, 23])
            cylinder(h=6, d=6.7, $fn=50);

            translate([20, 0, 27.0])
            rotate([180,0,0])
            nut(nuts[4], false);

            translate([27, 0, 20])
            cylinder(h=6, d=3, $fn=50);
        }

        translate([20, 0, 20])
        cylinder(h=6, d=6, $fn=50);

        translate([20, 0, 23])
        cylinder(h=6, d=6.7, $fn=50);

        translate([20, 0, 27.0])
        rotate([180,0,0])
        nut(nuts[4], false);

        translate([27, 0, 20])
        cylinder(h=6, d=3, $fn=50);


        Cutouts();

        mirror([0,1,0])
        Cutouts();

        mirror([1,0,0])
        Cutouts();

        mirror([1,0,0])
        mirror([0,1,0])
        Cutouts();
    }
}

SwitchPlate3(4.75);