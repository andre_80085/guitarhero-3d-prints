include <Round-Anything/polyround.scad>

width = 17.5;
length = 26;
squareLength = length - width/2;
roundLength = width/2;
rounding = 1.5;

module BodyTriangle()
{
    points = [
        
        [0, squareLength, 0],
        [0, rounding, 0],
        [0, 0, rounding],
        [rounding, 0, 0],

        //+X -Y
        [width - rounding, 0, 0],
        [width, 0, rounding],
        [width, rounding, 0],
        [width, squareLength, 0],

        [width/2, length, roundLength*2],
    ];
    x = polyRound(points,32);
    polygon(x);
}

BodyTriangle();

translate([-2, 0,0])
square([1, length], center=false);
translate([0,length, 0])
square([1, length], center=false);

translate([roundLength, squareLength, 0])
#circle(r=roundLength, $fn=64);