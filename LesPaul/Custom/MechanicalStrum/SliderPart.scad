
module SliderPart(d, w)
{
    r = 0.25;
    railH = 12;
    h2 = railH + 12;
    fn1 = $preview ? 50 : 100;
    d_marging = 0.05;
    d2 = d + d_marging + (r*2);

    height = d + 3;
    width = (w * 2) + d + 3;
    width2 = (d + 4);
    xoffset = (d/2) + 2;
    xoffset2 = (w/2) - 2;
    xoffset3 = (w/2) - d - 1;
    difference()
    {
        union()
        {
            translate([0, 0, 3])
            cube([width, height, 6], center=true);

            translate([0, 1.5, 3])
            cube([10, height, 6], center=true);

            translate([-w, 0, 0])
            cylinder(h=14, d=8, $fn=fn1);

            translate([w, 0, 0])
            cylinder(h=14, d=8, $fn=fn1);
        }

        translate([-w,0,-1])
        cylinder(h=height * 2, d=6.05, $fn=fn1);

        translate([w,0,-1])
        cylinder(h=height * 2, d=6.05, $fn=fn1);

        translate([0,-3.5, 2.99])
        cube([10.5, height, 6.1], center=true);

        translate([-3, 3.5, 0])
        cylinder(h=20, d=2.05, center=true, $fn=fn1);

        translate([3, 3.5, 0])
        cylinder(h=20, d=2.05, center=true, $fn=fn1);

        translate([5,-5,2.99])
        rotate([0,0,45])
        cube([8,8,6.1], true);

        mirror([1,0,0])
        translate([5,-5,2.99])
        rotate([0,0,45])
        cube([8,8,6.1], true);
        
    }
    
}

module SliderCenterPart(d, w = 7)
{
    color("#33CC7B")
    difference()
    {
        union()
        {
            translate([0, 0, 0])
            SliderPart(d, w);

            /*
            mirror([0, 0, 1])
            translate([-w, 0, 0])
            SliderPart(d, w);
            */
        }

        
    }
}

SliderCenterPart(6, 14);