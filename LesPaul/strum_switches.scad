include <./commonVars.scad>

switchLength1 = 40;
switchLength2 = 12.5;
switchLength3 = 8;

switchWidth1 = 18;
switchWidth2 = 12.5;
switchWidth3 = 5;

switchHeight1 = switchBaseHeight;
switchHeight2 = 3.25;
switchHeight3 = 3.5;

screwHoleD = 5;
screwHoleH = 6;
screwHoleOffsetZ = -switchHeight1/2 + screwHoleH/2;
screwHoleOffsetY = switchLength1/2 + screwHoleD/2;
screwHoleOffsetX = switchWidth1/2 + screwHoleD/2;

switchOffsetY1 = (6.5/2) + (switchLength2/2);

module APC_Switch(offsetY)
{
    z1 = switchHeight1/2 + switchHeight2/2;
    z2 = switchHeight1/2 + switchHeight2 + switchHeight3/2;

    translate([0, offsetY, 0])
    {
        color("#404040")
        translate([0, 0, z1])
        cube([switchWidth2, switchLength2, switchHeight2], center=true);

        color("white")
        translate([0, 0, z2])
        cube([switchWidth3, switchLength3, switchHeight2], center=true);
    }

    
}

module DummySwitch()
{
    color("#202020")
    union()
    {
        cube([switchWidth1, switchLength1, switchHeight1], center=true);

        translate([0, 0, screwHoleOffsetZ])
        {
            translate([0, screwHoleOffsetY, 0])
            cube([screwHoleD, screwHoleD, screwHoleH], center=true);

            translate([0, -screwHoleOffsetY, 0])
            cube([screwHoleD, screwHoleD, screwHoleH], center=true);

            translate([screwHoleOffsetX, 0, 0])
            cube([screwHoleD, screwHoleD, screwHoleH], center=true);

            translate([-screwHoleOffsetX, 0, 0])
            cube([screwHoleD, screwHoleD, screwHoleH], center=true);
        }
    }

    APC_Switch(switchOffsetY1);
    APC_Switch(-switchOffsetY1);
}

DummySwitch();