use <./Strum/Strum.scad>
use <./Strum/StrumExtension.scad>
use <./Strum/StrumpinGuide.scad>
use <./../../strum_bodymount.scad>
use <NopSCADlib/core.scad>

use <NopSCADlib/utils/layout.scad>
include <NopSCADlib/vitamins/springs.scad>

use <./strum_pcb_mount.scad>
use <./../../../util/MxSwitch.scad>
use <./Plate.scad>

showBodyMounts = false;
showBody = true;
showStrum = true;
display_switch = true;
display_center_part = false;

rotatePreview = 17; //17.67;
switchPressed = 2;
switchSpacing = 4.75;
switchOffsetZ = 18.75;

module Preview()
{
    StrumPCBMountOffetZ = 4;
    PinOffsetZ = 2;
    StrumOffetZ = 7.75;

    color("#707070")
    translate([0,0, PinOffsetZ])
    rotate([0, 90, 0])
    cylinder(d=4, h=85, center=true, $fn=32);
    
    pinOffsetX = 29.75;
    color("#303030")
    translate([pinOffsetX, 0, PinOffsetZ])
    rotate([0, 90, 0])
    PinGuide(6.5, 9, 4, 0.5, 64);

    color("#303030")
    translate([-pinOffsetX, 0, PinOffsetZ])
    rotate([0, 90, 180])
    PinGuide(6.5, 9, 4, 0.5, 64);


    if(showBody)
    {
        translate([0,0,0])
        rotate([0, 0, 90])
        StrumBodyMount();
    }

    if(showBodyMounts)
    {
        translate([-37,0,4])
        StrumPCBMountPart();

        mirror([1,0,0])
        translate([-37,0,4])
        StrumPCBMountPart();
    }

    translate([0,0,2])
    rotate([rotatePreview,0,0])
    translate([0,0, -2])
    translate([0,0, StrumOffetZ])
    rotate([0, 180, 90])
    {
        if(showStrum)
        {
            color("#AAAA00")
            StrumBody();
        }
        

        translate([0, 27.625, StrumOffetZ - PinOffsetZ])
        rotate([90,0,180])
        color("#18FFFF")
        StrumSpacer(5.25);
        
        if(display_center_part)
        color("#00B0FF")
        StrumExtension2();
        
        /*
        translate([0,20,0])
        StrumSpringMount(length = 5, d=3.9);
        */

        /*
        mirror([0,1,0])
        translate([0,20,0])
        StrumSpringMount(length = 5, d=3.9);
        */
        
        translate([0,20,0])
        color("#4AE51A")
        StrumFeets(true);
        
    }

    /*
    if(display_switch)
    {
        Switchs();

        mirror([0,1,0])
        Switchs();
    }
    */

    /*
    translate([20,0, 5])
    comp_spring(["peg_spring",  5,  0.5,  18.75,  26, 1, false, 0, "silver"], l=(18.75 - 1));

    mirror([1,0,0])
    translate([20,0, 5])
    comp_spring(["peg_spring",  5,  0.5,  18.75,  26, 1, false, 0, "silver"], l=(18.75 - 1));
    */

    translate([0,0,24.5])
    rotate([180,0,0])
    SwitchPlate3(switchSpacing);

    translate([0,0, switchOffsetZ])
    rotate([180,0,0])
    SwitchMount2(spacing = switchSpacing);

    
    //translate([0,0, 20.8 + 6.70])
    //SwitchMount();

    /*
    translate([0,21.3,14])
    SwitchMount();
    */

    /*
    translate([20,0,25])
    rotate([180,0,0])
    SpringHolder(outer = 3.9, length=5);
    */

    /*
    color("orange")
    translate([0,0,2])
    rotate([-20,0,0])
    union()
    {
        cube([1,1,100], center=true);
    }

    color("orange")
    translate([0,0,2])
    rotate([20,0,0])
    union()
    {
        cube([1,1,100], center=true);
    }

    color("orange")
    translate([20,0,2])
    rotate([rotatePreview,0,0])
    union()
    {
        cube([5,3,100], center=true);
    }
    */

    /*
    translate([20, -(30/2), 23])
    rotate([-90,0,0])
    comp_spring(["peg_spring",  7,  0.5,  13,  8, 1, false, 0, "silver"], l=(5.5));

    translate([20, (30/2), 23])
    rotate([90,0,0])
    comp_spring(["peg_spring",  7,  0.5,  13,  8, 1, false, 0, "silver"], l=(13));
    */

    //cube([4, 13, 4], center=true);

    translate([0,0,-18.75])
    cube([1, 10, 0.25], center=true);

    translate([0,0,-19])
    cube([1, 13, 0.25], center=true);
}

module SwitchMount2(spacing = 1)
{
    SwitchMount3(spacing);

    offset = 7 + spacing/2;

    if(display_switch)
    {
        translate([0,offset, 1.75/2])
        SwitchFull(pressed = switchPressed, squareMount = false);

        translate([0,-offset, 1.75/2])
        SwitchFull(pressed = switchPressed, squareMount = false);
    }
}

module Switchs()
{
    translate([0,10.5,20.85])
    {
        rotate([180,0,0])
        //color("#5733CC")
        SwitchFull(pressed = switchPressed, squareMount = false);

        /*
        color("lightblue")
        translate([0,0,-9.35 + switchPressed])
        union()
        {
            translate([0,0,4.5/4])
            cube([4.5,4.5,4.5/2], center=true);

            rotate([0,90,0])
            cylinder(h=4.5, d=4.5, center=true, $fn=48);
        }
        */
        

        color("red")
        translate([0,0, 6.58])
        cube([19.1, 19.1, 1.45], center=true);
    }
}

module SwitchMount()
{
    color("#88b719")
    difference()
    {   
        cube([22, 44, 4], center=true);

        _SwitchMountCut();

        mirror([0,1,0])
        _SwitchMountCut();

        translate([8,0,0])
        cylinder(h=5, d=3.1, center=true, $fn=48);
    
        translate([-8,0,0])
        cylinder(h=5, d=3.1, center=true, $fn=48);
    }
}

module _SwitchMountCut()
{
    translate([0, 11.5, 0])
    cube([19.5, 19.5, 1.9], center=true);

    translate([0,11.5 + 2,0])
    cube([19.5, 19.5, 1.9], center=true);

    translate([0,11.5 + 1.5,0])
    cube([17, 20, 6], center=true);
}

Preview();
//SwitchPlate();

translate([10,10,0])
cube([1,1,20]);

translate([20,30,0])
cube([1,1,26.5]);