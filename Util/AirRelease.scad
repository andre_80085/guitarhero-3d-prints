
module AirRelease(l = 5, h = 1)
{
    rotate([270,0,0])
    linear_extrude(height = l, center = true, convexity = 10, twist = 0)
    polygon(points=[[-0.5,0],[0,h],[0.5,0]]);
}

AirRelease();