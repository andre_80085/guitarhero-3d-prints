include <StrumVars.scad>

module PinGuide(d1, d2, d3, outerLength = 1, fn = 32)
{
    ht = ProfileThickness + outerLength;

    translate([0, 0, ht/2])
    difference()
    {
        union()
        {
            cylinder(h=ht, d=d1, center=true, $fn=fn);
            translate([0, 0, (outerLength/2) + (ht/2) - outerLength])
                cylinder(h=outerLength, d=d2, center=true, $fn=fn);
        }
        cylinder(h=ht*2, d=d3, center=true, $fn=fn);
    }

}

module PinGuideWithText(d1, d2, d3, outerLength = 1, fn = 32)
{
    PinGuide(d1, d2, d3, outerLength, fn);
    /*
    translate([0, - d2/2 - 2, 0])
    linear_extrude(height = 0.1, center = false, convexity = 10, twist = 0)
    text(str(d2,"mm, ",d1, "mm"), size=1, halign="center");

    translate([0, - d2/2 - 3.5, 0])
    linear_extrude(height = 0.1, center = false, convexity = 10, twist = 0)
    text(str(outerLength," mm"), size=1, halign="center");
    */
}

//6.5, 9, 4, 1, 64

translate([-10, 0, 0])
PinGuideWithText(PinHole, PinHoleGuideD, 4, 0.5, 64);

PinGuideWithText(PinHole, PinHoleGuideD, 4, 1, 64);

translate([10, 0, 0])
PinGuideWithText(PinHole, PinHoleGuideD, 4, 1.5, 64);

translate([20, 0, 0])
PinGuideWithText(PinHole, PinHoleGuideD, 4, 2, 64);

translate([30, 0, 0])
PinGuideWithText(PinHole, PinHoleGuideD, 4, 2.5, 64);

