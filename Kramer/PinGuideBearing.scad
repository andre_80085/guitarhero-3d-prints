pinD = 4;
GuideD1 = 9;
GuideD2 = 6.5;

module AdjustedPin(offset)
{
    difference()
    {
        union()
        {
            cylinder(h=1, d=GuideD1, $fn=64);
            cylinder(h=4.25, d=GuideD2, $fn=64);
        }

        translate([offset,0,0])
        cylinder(h=15, d=pinD, $fn=64);
    }
}


translate([0, 8, 0])
{
    AdjustedPin(1);
    translate([12,0,0])
    AdjustedPin(0.75);

    translate([-12,0,0])
    AdjustedPin(0.5);
}

translate([0, -8, 0])
{
    AdjustedPin(1);
    translate([12,0,0])
    AdjustedPin(0.75);

    translate([-12,0,0])
    AdjustedPin(0.5);
}