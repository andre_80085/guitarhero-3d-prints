include <Round-Anything/polyround.scad>

pinDistanceY = 74;
pinDistanceX = 32;
pinD1 = 4;
pinD2 = 2;
height1 = 16;

extrudedZ = 4;

strumHoleWidth = 22;
strumHoleLength = 65;

module BodyMoldPin(xMod, yMod, rot, long_ping = false)
{
    w = (7.5)/2 - (pinD1/2) + 0.5;
    x = pinD1/2 + w/2 - 0.5;
    
    translate([xMod * pinDistanceX/2, yMod * pinDistanceY/2, height1/2])
    difference()
    {
        union()
        {
            translate([0,0,-1])
            cylinder(d=pinD1, h=height1 + 2, center=true, $fn=48);

            rotate([0,0,rot])
            {
                y = -height1/2 + 5/2 - 0.5;
                

                if(long_ping)
                {
                    translate([-x, 0, -height1/2])
                    cube([w, 1, 8], center=true);
                }
                else
                {
                    translate([-x, 0, y])
                    cube([w, 1, 4], center=true);
                }
                
                translate([x, 0, y])
                cube([w, 1, 4], center=true);

                translate([0, x, y])
                cube([1, w, 4], center=true);

                translate([0, -x, y])
                cube([1, w, 4], center=true);
            }
        }

        translate([0,0,height1/4])
        cylinder(d=pinD2, h=height1, center=true, $fn=48);
    }
}

module StrumExtrude()
{
    y = 101/2;
    x = 30/2;
    r = x;

    points = [
        [-x, y, r],
        [x, y, r],
        [x, -y, r],
        [-x, -y, r]
    ];

    polyRoundExtrude(points,4,2,0,16,5);
}

module StrumCutout()
{
    y = (101 - 5)/2;
    x = (30 - 6)/2;
    r = x;

    points = [
        [-x, y, r],
        [x, y, r],
        [x, -y, r],
        [-x, -y, r]
    ];

    polyRoundExtrude(points,4.01,-4,0,16,10);
}

module PinHolder()
{
    offset = strumHoleLength/2;

    difference()
    {
        union()
        {
            translate([0, offset + 0.75, -1])
            cube([20,1.5,6], center=true);

            translate([0, offset + 5, -1])
            cube([18,1,6], center=true);

            translate([0, offset + 11, -1])
            cube([15,1,6], center=true);

            translate([0, offset + 17/2, -3])
            cube([1,17,4], center=true);
        }

        translate([0, offset + 17/2, 2])
        rotate([90,0,0])
        cylinder(d=4, h=10, $fn=48);
    }
    
}

module StrumBodyMount()
{
    color("#404040")
    union()
    {
        difference()
        {
            union()
            {
                translate([0,0,-1])
                cube([pinDistanceX + 50, pinDistanceY + 50, 2], center=true);

                translate([0,0,-2])
                rotate([0, 180, 0])
                StrumExtrude();
            }
            
            translate([0,0,-4])
            StrumCutout();

            cube([strumHoleWidth, strumHoleLength, 50], center=true);
        }

        BodyMoldPin(1,1,0, true);
        BodyMoldPin(-1,1, 180, true);
        BodyMoldPin(1,-1, 0, true);
        BodyMoldPin(-1,-1, 180, true);

        PinHolder();
        mirror([0, 1, 0])
        PinHolder();

    }


    
}

StrumBodyMount();