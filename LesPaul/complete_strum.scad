use <./strum_switches.scad>
use <./strum_pcb.scad>
use <./../Common/strum_pcb_mount.scad>
use <./Strum/Strum.scad>
use <./Strum/StrumpinGuide.scad>
use <./strum_bodymount.scad>
include <./commonVars.scad>

module CompleteStrumReference(
    use_pcb = true, use_switch = true, use_mount1 = true, 
    use_mount2 = true, use_pin = true, use_strum = true, 
    use_pinGuide1 = true, use_pinGuide2 = true, use_body = true)
{
    StrumPCBMountOffetZ = 4;
    PinOffsetZ = 2;
    StrumOffetZ = 7.75;
    pinOffsetX = 29.75;

    if(use_pcb)
    {
        translate([0,0,20 - 0.75])
        rotate([180,0,0])
        StrumPCB();
    }

    if(use_switch)
    {
        translate([0,0,20 - switchBaseHeight/2 - 1.5])
        rotate([180,0,0])
        DummySwitch();
    }

    if(use_mount1)
    {
        translate([PCBOffetX,0,StrumPCBMountOffetZ])
        rotate([0, 0, 0])
        StrumPCBMountPart();
    }

    if(use_mount1)
    {
        translate([-PCBOffetX,0,StrumPCBMountOffetZ])
        rotate([0, 0, 0])
        StrumPCBMountPart();
    }

    if(use_pin)
    {
        color("#707070")
        translate([0,0, PinOffsetZ])
        rotate([0, 90, 0])
        cylinder(d=4, h=85, center=true, $fn=32);
    }

    if(use_strum)
    {
        color("#AAAA00")
        translate([0,0, StrumOffetZ])
        rotate([0, 180, 90])
        StrumBody();
    }
    
    if(use_pinGuide1)
    {
        color("#303030")
        translate([pinOffsetX, 0, PinOffsetZ])
        rotate([0, 90, 0])
        PinGuide(6.5, 9, 4, 0.5, 64);
    }

    if(use_pinGuide2)
    {
        color("#303030")
        translate([-pinOffsetX, 0, PinOffsetZ])
        rotate([0, 90, 180])
        PinGuide(6.5, 9, 4, 0.5, 64);
    }

    if(use_body)
    {
        translate([0,0,0])
        rotate([0, 0, 90])
        StrumBodyMount();
    }
}

CompleteStrumReference();
/*
translate([0,28,10])
cube([3,3,20], center=true);
*/