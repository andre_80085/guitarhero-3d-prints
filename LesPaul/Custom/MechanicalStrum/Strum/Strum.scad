include <StrumVars.scad>
use <StrumProfile.scad>
use <./../../../../util/AirRelease.scad>
use <NopSCADlib/core.scad>
use <NopSCADlib/utils/gears.scad>

bodyOffsetZ = 0;
feetLengthZ = 15;
width2 = 36;
bodyLength = 64.5;
gap = 28;
centerCutoutH = 9 - (feetHeight + feetOffset);
centerZ = centerCutoutH + (feetHeight + feetOffset);

CenterExtraLength = 13;
CenterCutoutHeight = 4.5;

//fns
centerFn = 32 + 10;
guidepinFn = 64 + 10;
innerWallsFn = 32 + 10;
centerCutoutFn = 32 + 10;

module Extruded(length)
{
    rotate([90,0,0])
        linear_extrude(height = length, center = true, convexity = 10, twist = 0)
            FullProfile();
}

module GuidepinCutout()
{
    translate([0, 0, 2.5 + (PinHole/2)])
        rotate([90, 0, 0])
            cylinder(h=bodyLength*2, d=PinHole, center=true, $fn=guidepinFn);
}

module InnerBodyCutout(length)
{
    rotate([90,0,0])
        linear_extrude(height = length, center = true, convexity = 10, twist = 0)
            StrumProfileCutout();
}

module InnerBodyWalls()
{
    points = [
        [-4.5, -1, 0],
        [-4.5, 0, 0],
        [-2, 10, 3],
        //[0, 10.5, 1],
        [2, 10, 3],
        [4.5, 0, 0],
        [4.5, -1, 0]
    ];

    difference()
    {
        resize(newsize=[bodyWidth, 1, 22], auto=false)
        {
            difference()
            {
                Extruded(1);
                cube([1000, 1000, 0], center=true);
            }
        }


        rotate([90,0,00])
            linear_extrude(height = 2, center = true, convexity = 10, twist = 0)
                polygon(polyRound(points,innerWallsFn));
    }
}

module GuidepinOuter(yMod)
{
    y = (bodyLength/2);
    z = 2.5 + (PinHole/2);
    translate([0, y * yMod, z])
        rotate([90,0,0])
            cylinder(h=1.5, d=PinHoleGuideD, center=true, $fn=guidepinFn);
}

module StrumGear()
{
    rotate([90,0,0])
    {
        linear_extrude(height = 5, center = true, convexity = 10, twist = 0)
        difference()
        {
            union()
            {
                difference()
                {
                    m = 1;
                    z = 28;
                    translate([0, 6, 0])
                    involute_gear_profile(m, z, pa = 20, clearance = undef, steps = 20);

                    translate([0, 15, 0])
                    square([40, 30], center=true);

                    translate([bodyWidth/2 + 2.5, 13.5, 0])
                    square([5, 30], center=true);

                    translate([bodyWidth/-2 - 2.5, 13.5, 0])
                    square([5, 30], center=true);
                }

                translate([0,2,0])
                square([bodyWidth - (bodyThickness*2), 4], center=true);

                translate([0,0,0])
                square([10, 10], center=true);

                translate([0,5,0])
                circle(d=10, $fn=48);
            }

            translate([0,5.75,0])
            circle(d=4.05, $fn=48);

            //Reductions
            translate([0,-3,0])
            circle(d=3, $fn=48);

            translate([5,-2,0])
            circle(d=3, $fn=48);

            translate([-5,-2,0])
            circle(d=3, $fn=48);
        }
    }
}

module StrumBody()
{
    union()
    {
        difference()
        {
            //Main part
            Extruded(bodyLength);

            InnerBodyCutout(bodyLength - (2*ProfileThickness) );

            GuidepinCutout();
            GuidepinOuter(1);
            GuidepinOuter(-1);

            //Air release for printing
            translate([0,10,-0.01])
            rotate([0,180,90])
            AirRelease(l=bodyWidth*2);

            translate([0,20,-0.01])
            rotate([0,180,90])
            AirRelease(l=bodyWidth*2);

            mirror([0,1,0])
            {
                translate([0,10,-0.01])
                rotate([0,180,90])
                AirRelease(l=bodyWidth*2);

                translate([0,20,-0.01])
                rotate([0,180,90])
                AirRelease(l=bodyWidth*2);
            }

            //Drain hole
            translate([0,0, 21.5])
            rotate([90,0,0])
            cylinder(h=bodyLength*2, d=0.3, center=true, $fn=48);
        }
        
        //Supporting walls inside
        translate([0, gap/2 + 0.5, 0])
            InnerBodyWalls();
        translate([0, (-gap/2 - 0.5), 0])
            InnerBodyWalls();

        //Supporting walls near gear
        translate([0, 3.01, 0])
            InnerBodyWalls();
        
        translate([0, -3.01, 0])
            InnerBodyWalls();
    }
}

color("yellow")
StrumBody();
