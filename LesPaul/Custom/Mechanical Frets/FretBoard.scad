use <./../../../util/MxSwitch.scad>
use <./../../../Common/MxFret.scad>
use <NopSCADlib/utils/round.scad>
include <Round-Anything/polyround.scad>

fretSpacing = 24;
travelStopW1 = 9;
travelStopW2 = 3.5;
travelStopH = 1;

module SupportCylinders()
{
    for(i = [-2 : 1 : 2])
    {
        y = i * fretSpacing;

        translate([0,y,-6.5/2])
        difference()
        {
            union()
            {
                cylinder(h=6.5, d=8, center=true, $fn=48);
                if(abs(i) != 1)
                cube([38, 1, 6.5], center=true);
            }
            cylinder(h=100, d=6, center=true, $fn=48);
        }
        
    }
}

module BackSupport()
{
    union()
    {
        translate([0,0,-1.5])
        SupportCylinders();

        translate([7,0,-4.75])
        cube([1, 150, 6.5], center=true);

        translate([-7,0,-4.75])
        cube([1, 150, 6.5], center=true);


        translate([0,75,-4.75])
        cube([38, 1, 6.5], center=true);

        translate([0,75,-2])
        cylinder(h=12, d=8.5, center=true, $fn=50);

        mirror([0,1,0])
        {
            translate([0,75,-4.75])
            cube([38, 1, 6.5], center=true);

            translate([0,75,-2])
            cylinder(h=12, d=8.5, center=true, $fn=50);
        }
        
    }
}

module FretShape(x = 27.5, y = 18, h=5)
{
    points = [
        [x/2, y/2, 1.5],
        [-x/2, y/2, y/2],
        [-x/2, -y/2, y/2],
        [x/2, -y/2, 1.5]
    ];

    linear_extrude(height = h, center = false, convexity = 10, twist = 0)
    polygon(
        polyRound(points,16)
    );
}

module NeckMold()
{
    y1 = 140;
    x1 = 38;
    x2 = 42;

    t1 = 2.5;
    t2 = 1;

    h1 = 15;
    h2 = 12.5;

    points = [
        [-x1/2, y1/2, 0],
        [x1/2, y1/2, 0],
        [x2/2, -y1/2, 0],
        [-x2/2, -y1/2, 0]
    ];

    points2 = [
        [-x1/2 + t1, y1/2 + 0.1, 0],
        [x1/2 - t1, y1/2 + 0.1, 0],
        [x2/2 - t1, -y1/2 - 0.1, 0],
        [-x2/2 + t1, -y1/2 - 0.1, 0]
    ];

    difference()
    {
        union()
        {
            difference()
            {
                linear_extrude(height = h1, center = false, convexity = 10, twist = 0)
                polygon(
                    polyRound(points,16)
                );

                translate([0,0,h1-h2])
                linear_extrude(height = h2+0.01, center = false, convexity = 10, twist = 0)
                polygon(
                    polyRound(points2,16)
                );

                for(i = [-2 : 1 : 2])
                {
                    y = i * fretSpacing;

                    translate([0, y, -1])
                    FretShape();
                }

                
            }

            for(i = [-2 : 1 : 2])
            {
                y = i * fretSpacing;

                translate([0, y, h1-h2])
                difference()
                {
                    union()
                    {
                        FretShape(x=29.5, y=20, h=14.5);

                        translate([0,0,14.5/2])
                        cube([36, 4, 14.5], center=true);


                        translate([2.5, 0,0])
                        {
                            translate([16.5/2,9 + 4/2, 14.5/2])
                            cube([1, 4, 14.5], center=true);

                            translate([-16.5/2,9 + 4/2, 14.5/2])
                            cube([1, 4, 14.5], center=true);
                        }

                        mirror([0,1,0])
                        translate([2.5, 0,0])
                        {
                            translate([16.5/2,9 + 4/2, 14.5/2])
                            cube([1, 4, 14.5], center=true);

                            translate([-16.5/2,9 + 4/2, 14.5/2])
                            cube([1, 4, 14.5], center=true);
                        }

                        if(i == -2)
                        {
                            translate([2.5, -9 -4 + 0.5, 14.5/2])
                            cube([16.5, 1, 14.5], center=true);
                        }

                        if(i == 2)
                        {
                            mirror([0,1,0])
                            translate([2.5, -9 -4 + 0.5, 14.5/2])
                            cube([16.5, 1, 14.5], center=true);
                        }
                    }
                    
                    translate([0,0,-0.1])
                    FretShape(h=15);

                    translate([0,0,15/2])
                    cube([38, 2, 15], center=true);

                    translate([0,0,14.5 - 5/2 + 0.5])
                    cube([38, 8, 5], center=true);
                    
                }

                if(i != 2)
                {
                    translate([0, y + 12, 0])
                    {
                        cube([34, 1, 2], center=true);
                    }
                }
                
            }
        }

        translate([2.5,0,7.5 + 2.5])
        cube([15.5, 120, 15], center=true);
    }
}

module TravelStop(h = 1, w = 11)
{
    //w = 11;
    l = 13.75;
    l2 = 5;
    m = 1;

    union()
    {
        linear_extrude(height = h, center = false, convexity = 10, twist = 0)
        difference()
        {
            round(1)
            square([l, w], center=true);
        }
        

        translate([0,0,-2])
        linear_extrude(height = h + 2, center = false, convexity = 10, twist = 0)
        {
            translate([-l/2 + 2, 0])
            circle(d=2, $fn=50);

            translate([l/2 - 2, 0])
            circle(d=2, $fn=50);
        }
    }
    
}

module SwitchMount()
{
    x1 = 15.4 - 1;
    h1 = 2;
    h2 = 9.5;
    h3 = 5.1;

    difference()
    {
        union()
        {
            translate([0,0, h1/2])
            linear_extrude(height = h1, center = true, convexity = 10, twist = 0)
            difference()
            {
                union()
                {
                    round(1)
                    square([x1 - 0.5, (fretSpacing*5 - 2)], center=true);

                    for(i = [-2 : 1 : 2])
                    {
                        y = i * fretSpacing;

                        translate([-2, y,0])
                        round(2)
                        square([17, 17], center=true);

                        translate([x1/2 - 0.5, y,0])
                        circle(d=17, $fn=50);
                    }
                }

                for(i = [-2 : 1 : 2])
                {
                    y = i * fretSpacing;

                    translate([-2.54, y - 3.81,0])
                    circle(d=2, $fn=50);

                    translate([-5.08, y + 2.54,0])
                    circle(d=2, $fn=50);
                    
                    translate([0, y - (4*1.27),0])
                    circle(d=1.75, $fn=50);

                    translate([0, y + (4*1.27),0])
                    circle(d=1.75, $fn=50);
                }
            }

            translate([0,0,-(h2/2)])
            linear_extrude(height = h2, center = true, convexity = 10, twist = 0)

            for(i = [-2 : 1 : 2])
            {
                y = i * fretSpacing;

                translate([2.45, y, 0])
                circle(d=5.9, $fn=50);
            }

            translate([0,0,-(h3/2)])
            linear_extrude(height = h3, center = true, convexity = 10, twist = 0)
            for(i = [-2 : 1 : 2])
            {
                y = i * fretSpacing;

                translate([2.45, y, 0])
                circle(d=9.5, $fn=50);

                translate([-2, y, 0])
                round(1)
                square([4, 4], center=true);
            }
        }

        for(i = [-2 : 1 : 2])
        {
            y = i * fretSpacing;
            translate([0, y, -15])
            cylinder(d=3.9, h=30, $fn=50);
        }

        //Travel stop holes
        translate([0,0,-3.6])
        for(i = [-2 : 1 : 2])
        {
            l = 13.75;
            y = fretSpacing*i;

            translate([0,y,-2])
            linear_extrude(height = 10, center = false, convexity = 10, twist = 0)
            {
                translate([(-l/2) + 2, -12])
                circle(d=2.05, $fn=50);

                translate([(l/2) - 2, -12])
                circle(d=2.05, $fn=50);

                if(i == -2 || i == 2)
                {
                    translate([(-l/2) + 2, (4.625*i)])
                    circle(d=2.05, $fn=50);

                    translate([(l/2) - 2, (4.625*i)])
                    circle(d=2.05, $fn=50);
                }
            }
        }
    }
}

module Preview()
{
    frets = false;
    greenOnly = true;
    switches = true;
    showMount = true;
    showMold = false;
    showBackSupport = false;
    travelstop = true;
    
    if(showBackSupport)
    {
        color("#303030")
        BackSupport();
    }

    if(showMold)
    {
        translate([0,0, 18.25])
        rotate([0,180,0])
        NeckMold();
    }

    if(showMount)
    translate([-2.5 + 0.05, 0, 3.6])
    {
        color("#B24D4D")
        SwitchMount();
    }

    if(travelstop)
    {
        for(i = [-2 : 1 : 2])
        {
            y = fretSpacing*i;
            if(i != 2)
            translate([-2.45,y + 12, 5.6])
            color("#8D33CC")
            TravelStop(travelStopH, travelStopW1);

            if(i == -2 || i == 2)
            translate([-2.45,y + (4.625 * i), 5.6])
            color("#8D33CC")
            TravelStop(travelStopH, travelStopW2);
        }
    }

    if(switches || greenOnly)
    {
        //Preloaded distance
        p = 1;

        switchX = -13.25 + 18.5/2 + 1.5;
        translate([0, 0, 11.45])
        {
            
            translate([switchX, (2 * fretSpacing), 0])
            rotate([0,0,180])
            SwitchFull(pressed = p);
            

            if(greenOnly == false)
            {
                translate([switchX, (1 * fretSpacing), 0])
                rotate([0,0,180])
                SwitchFull(pressed = (p + 0.7) );

                translate([switchX, (0 * fretSpacing), 0])
                rotate([0,0,180])
                SwitchFull(pressed = (p + 1.2) );
                
                translate([switchX, (-1 * fretSpacing), 0])
                rotate([0,0,180])
                SwitchFull(pressed = (p + 1.8) );

                translate([switchX, (-2 * fretSpacing), 0])
                rotate([0,0,180])
                SwitchFull(pressed = (p + 3) );
            }
        }
    }
    

    if(frets || greenOnly)
    {
        translate([-13.25, 0, 6.8])
        {
            translate([0, (2 * fretSpacing), - 0])
            difference()
            {
                rotate([0,0,270])
                color("Green")
                FullMxFret();

                translate([-5,0,0])
                cube([40, 9, 30], center=false);
            }

            if(greenOnly == false)
            {
                translate([0, (1 * fretSpacing), -0.7])
                rotate([0,0,270])
                color("Red")
                FullMxFret();

                translate([0, (0 * fretSpacing), -1.2])
                rotate([0,0,270])
                color("Yellow")
                FullMxFret();

                translate([0, (-1 * fretSpacing), -1.8])
                rotate([0,0,270])
                color("Blue")
                FullMxFret();

                translate([0, (-2 * fretSpacing), -3])
                rotate([0,0,270])
                color("Orange")
                FullMxFret();
            }
        }
    }
}

Preview();

//For rendering STL
//TravelStop(travelStopH, travelStopW1);
//TravelStop(travelStopH, travelStopW2);
//SwitchMount();