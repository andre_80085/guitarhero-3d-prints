include <Round-Anything/polyround.scad>
include <StrumVars.scad>

bodyHeight = 5;
bodyUpperY = 0 + bodyHeight;

profileFn = 32;
circleFn = 256;

module FullProfile()
{
    union()
    {
        translate([0, bodyUpperY, 0])
            BodyTriangle4(0,-3);
        union(){
            LowerBody();
            translate([0,bodyUpperY,0])
            {
                UpperBodyHalfCircle();
            }
        }
    }
}

module LowerBody(xChange = 0, offset = 0)
{
    polygon([
        [(bodyWidth/2) - xChange, offset],
        [(bodyWidth/2) - xChange, bodyUpperY],

        [(-bodyWidth/2) + xChange, bodyUpperY],
        [(-bodyWidth/2) + xChange, offset]
    ]);
}

module UpperBodyHalfCircle(dChange = 0)
{
    difference(){
        circle(d=(bodyWidth - dChange), $fn=circleFn);
        translate([0, -bodyWidth/2, 0])
            square(size=[bodyWidth, bodyWidth], center=true);
    }
}

module BodyTriangle(xChange = 0, yChange = 0)
{
    points = [
        [-2 + xChange, bodyWidth/2 + 6 - yChange, 2],
        [-2.1 + xChange, bodyWidth/2 + 9.5 - yChange, 2],
        [2.1 - xChange, bodyWidth/2 + 9.5 - yChange, 2],
        [2 - xChange, bodyWidth/2 + 6 - yChange, 2],
        [(bodyWidth/2) - xChange, 0, 0],
        [(bodyWidth/-2) + xChange, 0, 0]
    ];
    polygon(polyRound(points,profileFn));
}

module BodyTriangle2()
{
    yChange = 1.25;
    xChange = bodyThickness;

    points = [
        [0, bodyWidth/2 + 6, 0],
        [(bodyWidth/2) - xChange, 0, 0],
        [(bodyWidth/-2) + xChange, 0, 0]
    ];
    polygon(polyRound(points,profileFn));
}

module StrumProfileCutout()
{
    xChange = bodyThickness;
    union()
    {
        translate([0, bodyUpperY, 0])
            BodyTriangle2();
        union(){
            LowerBody(xChange, -1);
            translate([0,bodyUpperY,0])
            {
                UpperBodyHalfCircle(xChange*2);
            }
        }
    }
}


difference()
{
    FullProfile();

    StrumProfileCutout();
};

/*
difference()
{
    union()
    {
        translate([0, bodyUpperY, 0])
            BodyTriangle4(0,-3);
        union(){
            LowerBody();
            translate([0,bodyUpperY,0])
            {
                UpperBodyHalfCircle();
            }
        }
    }
};
*/

module BodyTriangle4(xChange = 0, yChange = 0)
{
    points = [
        [(bodyWidth/-2) + 3.5 + xChange, 8, 0],
        [(bodyWidth/-2) + 8 + xChange, 16, 8],
        [-1.5 + xChange, bodyWidth/2 + 6 - yChange, 3],
        [-2.5 + xChange, bodyWidth/2 + 9 - yChange, 3],

        [2.5 - xChange, bodyWidth/2 + 9 - yChange, 3],
        [1.5 - xChange, bodyWidth/2 + 6 - yChange, 3],
        [(bodyWidth/2) - 8 + xChange, 16, 8],
        [(bodyWidth/2) - 3.5 - xChange, 8, 0]
    ];
    polygon(polyRound(points,profileFn));
}