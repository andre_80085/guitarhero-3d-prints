use <Switch.scad>

BodyWidth = 17.25;
BodySquareLength = 19 - 1.5;
TotalLength = BodySquareLength + BodyWidth/2;
BodyHeight = 15 - 3.8;
Thickness = 1.5;
InnerHeight = 13 - 3.8;
SidePartLength = 15;
SidePartHeight = 9;
SidePartHeightInner = 5;
SidePartWidth = 2;
AlignmentSize = 1.1;
RubberSlotDepth = 2.5;
RubberSlotLength = 8.5;
RubberSlotWidth = 1.1;
InnerCylinderDistance = 6.5/2;
InnerRoofWall = InnerHeight - 7;
EdgeSize = 2;
CornerSize = 2;

module BodyBase()
{
    translate([0,-BodyWidth/4,0])
    union()
    {
        cube([BodyWidth,BodySquareLength,BodyHeight], center=true);

        translate([0, (BodySquareLength/2), 0])
        cylinder(h=BodyHeight, d=BodyWidth, center=true, $fn=96);
    }

}

module SidePart(xMod)
{
    translate([(SidePartWidth/2 + BodyWidth/2)*xMod, -(BodySquareLength/4), -BodyHeight/2 + SidePartHeight/2])
    {
        difference()
        {
            cube([SidePartWidth, SidePartLength, SidePartHeight], center = true);

            translate([0,0, (SidePartHeight - SidePartHeightInner)/2])
            cube([SidePartWidth*2, SidePartLength - (2*AlignmentSize), SidePartHeightInner], center = true);

            translate([0,0, SidePartHeight/2 + RubberSlotDepth/-2 - SidePartHeightInner])
            cube([RubberSlotWidth, RubberSlotLength, RubberSlotDepth], center = true);
        }
        
    }
    
}

module InnerCylinder(yMod)
{
    translate([0, InnerCylinderDistance * yMod, 0])
    cylinder(d=4, h=BodyHeight, center=true, $fn=32);

    translate([0, yMod * (InnerCylinderDistance/2 + TotalLength/4), 0])
    cube([1, TotalLength/2 - InnerCylinderDistance, BodyHeight], center=true);

    translate([0, yMod * InnerCylinderDistance, 0])
    cube([BodyWidth, 1, BodyHeight], center=true);
}

module RoofCurve(width)
{
    translate([0, 0, BodyHeight/2 - 0.5])
    {
        difference()
        {
            cube([width, TotalLength, 1], center=true);

            translate([0, 0, 0.5 -(width*10/2)])
            rotate([90,0,0])
            cylinder(h=TotalLength, d=width*10, center=true, $fn=1000);
        }
    }
}

module SwitchCylinder()
{
    difference()
    {
        cylinder(h=3.60, d=5.50, $fn=64);
        cube([4.00, 1.10, 38], center=true);
        cube([1.10, 4.00, 38], center=true);
    }
}

module EdgeNegative(l = 100)
{
    difference()
    {
        cube([EdgeSize, EdgeSize, l], center=true);
        translate([EdgeSize/2, EdgeSize/2, -l/2])
        cylinder(h=l, r=EdgeSize, $fn=64);
    }
}

module CornerNegative()
{
    difference()
    {
        cube([CornerSize, CornerSize, CornerSize], center=true);
        translate([CornerSize/2, CornerSize/2, -CornerSize/2])
        sphere(r=CornerSize, $fn=64);
    }
}

module FretFull()
{
    difference()
    {
        union()
        {
            difference()
            {
                BodyBase();

                translate([0,0, -0.5 + (BodyHeight - InnerHeight) / -2])
                resize(newsize=[BodyWidth - (2*Thickness), (BodySquareLength + (BodyWidth/2)) - (2 * Thickness), InnerHeight + 1], auto=false)
                BodyBase();
            }
            
            //SidePart(1);
            //SidePart(-1);

            AlignmentHeight = BodyHeight - 3.8 - 3;
            translate([0,TotalLength/2, -(BodyHeight/2) + AlignmentHeight/2])
            cube([AlignmentSize, 2.5, AlignmentHeight], center=true);

            translate([0, -TotalLength/2, -(BodyHeight/2) + AlignmentHeight/2])
            cube([AlignmentSize, 2.5, AlignmentHeight], center=true);

            translate([0,-2.5, (BodyHeight/2) - 3.60 - ((BodyHeight - InnerHeight))])
            SwitchCylinder();
            //- ((BodyHeight - InnerHeight))
        }

        //RoofCurve(BodyWidth);

        //rotate([0,0,90])
        //RoofCurve(TotalLength);

        
        translate([-BodyWidth/2 + EdgeSize/2, -TotalLength/2 + EdgeSize/2, 0])
        EdgeNegative();

        translate([BodyWidth/2 - EdgeSize/2, -TotalLength/2 + EdgeSize/2, 0])
        rotate([0,0,90])
        EdgeNegative();

        translate([0, -TotalLength/2 + EdgeSize/2, BodyHeight/2 - EdgeSize/2])
        rotate([0,90,0])
        EdgeNegative();

        translate([BodyWidth/2 - EdgeSize/2, -TotalLength/2 + EdgeSize/2, BodyHeight/2 - EdgeSize/2])
        rotate([90,180,0])
        EdgeNegative();

        translate([-BodyWidth/2 + EdgeSize/2, -TotalLength/2 + EdgeSize/2, BodyHeight/2 - EdgeSize/2])
        rotate([90,90,0])
        EdgeNegative();

        translate([BodyWidth/2 - CornerSize/2, -TotalLength/2 + CornerSize/2, BodyHeight/2 - CornerSize/2])
        rotate([0, 90, 0])
        CornerNegative();

        translate([-BodyWidth/2 + CornerSize/2, -TotalLength/2 + CornerSize/2, BodyHeight/2 - CornerSize/2])
        rotate([0, 00, 0])
        CornerNegative();

        translate([0,TotalLength/2 - BodyWidth/2, BodyHeight/2 - EdgeSize/2])
        {
            d=BodyWidth/2;
            c = 32;
            rot = 180/c;
            for (a =[0:c]){
                rotate([0, 0, rot * a])
                translate([d - EdgeSize/2, 0, 0])
                rotate([-90,0,180])
                EdgeNegative(1);
                
            }
        }
        
    }
}

FretFull();


translate([0,-2.5,-10])
rotate([0,0,90])
SwitchFull();
