use <NopSCADlib/utils/gears.scad>

module LinearGear(teeths = 5)
{
    m = 1;
    z = teeths;
    w = 4;

    color("#00aab0")
    difference()
    {
        union()
        {
            rotate([270,0,90])
            linear_extrude(height = 10, center = true, convexity = 10, twist = 0)
            involute_rack_profile(m, z, w=w, pa = 20, clearance = undef, $fn=($preview ? 20 : 50));

            rotate([270,0,-90])
            linear_extrude(height = 10, center = true, convexity = 10, twist = 0)
            involute_rack_profile(m, z, w=w, pa = 20, clearance = undef, $fn=($preview ? 20 : 50));

            translate([0,0,6])
            cube([10, 18, 6], center=true);
            
        }

        translate([0,0,6.5])
        cube([10.1, 12.05, 5.01], center=true);

        translate([3, 0, 6.5])
        rotate([90,0,0])
        cylinder(h=30, d=2, center=true, $fn=($preview ? 50 : 100));

        translate([-3, 0, 6.5])
        rotate([90,0,0])
        cylinder(h=30, d=2, center=true, $fn=($preview ? 50 : 100));
    }
}

LinearGear(teeths = 4);