include <Round-Anything/polyround.scad>

StrumPCBPinX = 32;
StrumPCBPinY = 68;
StrumPinH = 15;
StrumHoleY = 60;

NeckSlotOuterX = 51.5;
NeckSlotLeftY = 47.5;
NeckSlotRightY = 69; //nice.
NeckSlotThickness = 1.5;
NeckSlotInnerX = 38;
NeckPCBSlotX = 27;
NeckPCBSlotY = 4.35;
NeckPCBSlotX2 = 5;
NeckExtrudeH0 = 13;
NeckExtrudeH1 = 14;
NeckOffsetY = (StrumHoleY/2) + 21.5 - NeckSlotThickness;

StrumStopSpacing = 27;
StrumStopSpacing2 = 2;

//Very rough, only for reference
module BodyFrontProfile()
{
    points = [
        [75, (StrumHoleY/2), 0],
        [75, (StrumHoleY/2) + 150, 0],
        [35, (StrumHoleY/2) + 90, 0],
        [(NeckSlotOuterX/2) - (NeckSlotThickness/2), NeckOffsetY + (NeckSlotRightY + 10), 0],
        [(NeckSlotOuterX/2) - (NeckSlotThickness/2), NeckOffsetY + 25, 0],
        [-(NeckSlotOuterX/2) + (NeckSlotThickness/2), NeckOffsetY + 25, 0],
        [-(NeckSlotOuterX/2) + (NeckSlotThickness/2), NeckOffsetY + NeckSlotLeftY, 0],
        [-(NeckSlotOuterX/2) - 20, NeckOffsetY + NeckSlotLeftY - 15, 0],
        [-(NeckSlotInnerX/2) + (NeckSlotThickness/2) - 60, NeckOffsetY + NeckSlotLeftY + 35, 0],
        [-(StrumPCBPinX/2) - 65, (StrumPCBPinY/2), -30],
        [-(StrumPCBPinX/2) - 90, -(StrumPCBPinY/2), 0],
        [-(StrumPCBPinX/2) - 105, -(StrumPCBPinY/2) - 65, -30],
        [-115, -130, -30],
        [-80, -150, -30],
        [-(StrumPCBPinX/2), -(StrumPCBPinY/2) - 120, 0],
        [0, -(StrumPCBPinY/2) - 120, 0],
        [(StrumPCBPinX/2), -(StrumPCBPinY/2) - 120, 0],
        [60, -(StrumPCBPinY/2) - 120, 0],
        [80, -(StrumPCBPinY/2) - 110, 30],
        [105, -(StrumPCBPinY/2) - 75, 30],
        [120, -(StrumPCBPinY/2) - 45, -30],
        [(StrumPCBPinX/2) + 85, -(StrumPCBPinY/2), -30]
    ];
    polygon(polyRound(points,16));
}

module StrumPin(xMod, yMod)
{
    x = (StrumPCBPinX/2) * xMod;
    y = (StrumPCBPinY/2) * yMod;

    translate([x, y, StrumPinH/2])
    cylinder(h=StrumPinH, d=4, center=true, $fn=32);
}

module NeckPCDHoleExtrude()
{
    x0 = (NeckPCBSlotX / 2)  - NeckPCBSlotX2;
    points = [
        [-x0, 3, 0],
        [-x0, 0, 0],
        [-x0 + 3, -3, 1],
        [x0 - 3, -3, 1],
        [x0, 0, 0],
        [x0, 3, 0],
    ];

    rotate([90,0,0])
    linear_extrude(height = NeckSlotThickness * 2, center = true, convexity = 10, twist = 0)
    polygon(polyRound(points,16));
}

module NeckSlotOuterExtrude()
{
    x0 = NeckSlotOuterX / 2;
    y0 = NeckSlotLeftY;
    y1 = NeckSlotRightY;
    t = NeckSlotThickness;
    roundness0 = 5; //estimated
    roundness1 = roundness0 - t;
    x1 = NeckSlotInnerX/2;
    x2 = NeckPCBSlotX/2;
    y2 = NeckPCBSlotY;
    t1 = 1.25;

    difference()
    {
        union()
        {
            //Higher height part
            linear_extrude(height = NeckExtrudeH1, center = false, convexity = 10, twist = 0)
            union()
            {
                //Right inner
                points2 = [
                    [x0 - t, y1, 0],
                    [x1, y1, 2],
                    [x1, 0, 0],
                    [x1 + t, 0, 0],
                    [x1 + t, y1 - t, 1],
                    [x0 - t, y1 - t, 0]
                ];
                polygon(polyRound(points2,16));

                //Left inner
                points3 = [
                    [-x0 + t, y0, 0],
                    [-x1, y0, 2],
                    [-x1, 0, 0],
                    [-x1 - t, 0, 0],
                    [-x1 - t, y0 - t, 1],
                    [-x0 + t, y0 - t, 0]

                ];
                polygon(polyRound(points3,16));

                //Supports between inner and outer walls
                //Not measured
                w = x0 - x1 - t;
                x3 = x1 + (w/2);
                translate([x3,10,0,])
                square([w, t], center=true);
                translate([x3,20,0,])
                square([w, t], center=true);
                translate([x3,30,0,])
                square([w, t], center=true);
                translate([x3,40,0,])
                square([w, t], center=true);
                translate([x3,50,0,])
                square([w, t], center=true);
                translate([x3,60,0,])
                square([w, t], center=true);

                translate([-x3,10,0,])
                square([w, t], center=true);
                translate([-x3,20,0,])
                square([w, t], center=true);
                translate([-x3,30,0,])
                square([w, t], center=true);
                translate([-x3,40,0,])
                square([w, t], center=true);
            }

            //Lower height part
            linear_extrude(height = NeckExtrudeH0, center = false, convexity = 10, twist = 0)
            union()
            {
                //Outer
                points = [
                    [-x0, 0, roundness0], //Outer lower left
                    [-x0, y0, 0], //Outer upper left
                    [-x0 + t, y0, 0], //Inner upper left
                    [-x0 + t, t, roundness1], //Inner lower left
                    
                    [x0 - t, t, roundness1], //Inner lower right
                    [x0 - t, y1 + 10, 0], //Inner upper right
                    [x0, y1 + 10, 0], //Outer upper right
                    [x0, 0, roundness0], //Outer lower right

                ];
                polygon(polyRound(points,16));

                //Left PCB holder
                points4 = [
                    [-x2, t, 0],
                    [-x2, -y2, 0],
                    [-x2 + NeckPCBSlotX2, -y2, 0],
                    [-x2 + NeckPCBSlotX2, -y2 + t1, 0],
                    [-x2 + t1, -y2 + t1, 0],
                    [-x2 + t1, t, 0]
                ];
                polygon(polyRound(points4,16));

                //Right PCB holder
                points5 = [
                    [x2, t, 0],
                    [x2, -y2, 0],
                    [x2 - NeckPCBSlotX2, -y2, 0],
                    [x2 - NeckPCBSlotX2, -y2 + t1, 0],
                    [x2 - t1, -y2 + t1, 0],
                    [x2 - t1, t, 0]
                ];
                polygon(polyRound(points5,16));
            }
        }

        translate([0, NeckSlotThickness/2, NeckExtrudeH0])
        NeckPCDHoleExtrude();
    }
}

translate([0,NeckOffsetY,0])
NeckSlotOuterExtrude();

BodyFrontProfile();

StrumPin(1,1);
StrumPin(-1,1);
StrumPin(-1,-1);
StrumPin(1,-1);

