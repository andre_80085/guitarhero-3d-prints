include <Round-Anything/polyround.scad>

width = 40;
width2 = 32;
depth = 8;
thichness1 = 2;
thichness2 = 1;
height = 15 - thichness1;
holeHeight = 12.5 - thichness1;
height2 = 17;
pinD = 4;

holeD1 = 6.5;
holeD2 = 5;
holeD3 = 2;
holeD4 = 4;
holeD5 = 1.5;

module MountLowerPart()
{
    w = width/2;
    w2 = 5;
    w3 = 4;
    w4 = (pinD/2);
    w5 = (pinD/4);
    h = thichness1;
    h2 = -(pinD/2);

    points = [
        
        [-w2, 0, 0],
        [-w3, h2, 0],
        [-w4, h2, 0],
        [-w4, 0, 2],
        [w4, 0, 2],
        [w4, h2, 0],
        [w3, h2, 0],
        [w2, 0, 0],

        [w, 0, 0],
        [w, h, 0],
        [-w, h, 0],
        [-w, 0, 0]
    ];

    rotate([90, 0, 0])
    linear_extrude(height = depth, center = true, convexity = 10, twist = 0)
    polygon(polyRound(points,16));
}

module MountSideCylinder(xMod)
{
    translate([width2/2 * xMod, 0, height/2 + thichness1])
    difference()
    {
        cylinder(h=height, d=holeD1, $fn=48, center=true);
        translate([0,0, holeHeight - height])
        cylinder(h=holeHeight, d=holeD2, $fn=48, center=true);

        cylinder(h=height*2, d=holeD3, $fn=48, center=true);
    }
}

module StrumPCBMountPart()
{
    color("#202020")
    rotate([0,0,90])
    union()
    {
        difference()
        {
            MountLowerPart();

            translate([width2/2, 0, 0])
            cylinder(h=holeHeight, d=holeD2, $fn=48, center=true);

            translate([-width2/2, 0, 0])
            cylinder(h=holeHeight, d=holeD2, $fn=48, center=true);
        }


        translate([0, 0, height/2 + thichness1])
        {
            xOffset = 6.25 + holeD5/2;

            translate([xOffset,0,0])
            cube([12.5, thichness2, height], center=true);

            translate([-xOffset,0,0])
            cube([12.5, thichness2, height], center=true);
        }

        MountSideCylinder(1);
        MountSideCylinder(-1);


        translate([0, 0, height2/2 + thichness1])
        difference()
        {
            cylinder(h=height2, d=holeD4, $fn=48, center=true);
            cylinder(h=height2 + 1, d=holeD5, $fn=48, center=true);
        }
    }
}


StrumPCBMountPart();