include <Round-Anything/polyround.scad>
use <./../../../util/AirRelease.scad>

width = 40;
width2 = 32;
depth = 8;
thichness1 = 2;
thichness2 = 8;
height = 15;
holeHeight = 15 - thichness1;
height2 = 17;
pinD = 4;

holeD1 = 6.5;
holeD2 = 4.15;
holeD3 = 2.15;

module MountLowerPart()
{
    fn1 = $preview ? 16 : 50;

    w = width/2;
    w2 = 5;
    w3 = 4;
    w4 = (pinD/2);
    w5 = (pinD/4);
    h = thichness1;
    h2 = -(pinD/2);

    points = [
        
        [-w2, 0, 0],
        [-w3, h2, 0],
        [-w4, h2, 0],
        [-w4, 0, 2],
        [w4, 0, 2],
        [w4, h2, 0],
        [w3, h2, 0],
        [w2, 0, 0],

        [w, 0, 0],
        [w, h, 0],
        [-w, h, 0],
        [-w, 0, 0]
    ];

    rotate([90, 0, 0])
    linear_extrude(height = depth, center = true, convexity = 10, twist = 0)
    polygon(polyRound(points,fn1));
}

module ExtendedSideMount()
{
    fn1 = $preview ? 50 : 100;
    translate([width/2 -depth/2, 0, 0])
    rotate([0,0,-45])
    difference()
    {
        l = 15;
        union()
        {
            
            translate([0,-2,0])
            cube([l, 4, 15 + 1.5], center=false);

            translate([l, 0, 20.5/2 -4])
            cylinder(h=20.5, d=depth, $fn=fn1, center=true);
        }

        translate([l, 0, 20.5/2 -4])
        cylinder(h=20.5*2, d=3, $fn=fn1, center=true);

        translate([l, 0, -0.55])
        cylinder(h=7, d=6, $fn=fn1, center=true);

        translate([l,0,16.55])
        AirRelease(l = depth*2);
    }
}

module StrumPCBMountPart()
{
    fn1 = $preview ? 50 : 100;

    translate([0,0,0])
    color("#202020")
    rotate([0,0,90])
    
    difference()
    {
        union()
        {
            MountLowerPart();

            translate([0, 0, 5.75])
            linear_extrude(height = (height/2), center = true, convexity = 10, scale=[0.99, (2/depth)])
            square([width, depth], center=true);

            translate([0, 0, 12.75])
            cube([width - 1, 2, height/2], center=true);

            translate([width2/2, 0, 7.5 + 0.75])
            {
                cylinder(h=height + 1.5, d=depth, $fn=fn1, center=true);
            }

            mirror([1,0,0])
            translate([width2/2, 0, 7.5 + 0.75])
            {
                cylinder(h=height + 1.5, d=depth, $fn=fn1, center=true);
            }
            
            ExtendedSideMount();
            mirror([1,0,0])
            ExtendedSideMount();
        }

        //Cutout on main cylinders
        translate([width2/2, 0, 6.5])
        {
            translate([0,0,10.1])
            rotate([0,0,-45])
            AirRelease(l = depth*2);

            cylinder(h=holeHeight + 0.1, d=holeD2, $fn=fn1, center=true);
            cylinder(h=height*2, d=holeD3, $fn=fn1, center=true);
        }

        mirror([1,0,0])
        translate([width2/2, 0, 6.5])
        {
            translate([0,0,10.1])
            rotate([0,0,-45])
            AirRelease(l = depth*2);

            cylinder(h=holeHeight + 0.1, d=holeD2, $fn=fn1, center=true);
            cylinder(h=height*2, d=holeD3, $fn=fn1, center=true);
        }
    }
}

StrumPCBMountPart();