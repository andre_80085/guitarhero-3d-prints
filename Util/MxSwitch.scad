
w = 14;
l = 15.6;
h = 10.70;
halfZ = h/2;

module SwitchBody()
{
    cornerH1 = 11.2 - h;
    cornerH2 = 5 - cornerH1;

    difference()
    {
        union()
        {
            cube([l, w, h], center=true);
            
            //Alignment cylinder
            translate([0, 0, -halfZ])
            cylinder(h=1.4*2, d=3.85, $fn=32, center=true);

            //Pins
            translate([5.08, -2.54, -halfZ -3.30/2])
            cube([0.20, 1, 3.30], center=true);
            translate([2.54, 3.81, -halfZ -3.30/2])
            cube([0.20, 1, 3.30], center=true);

            translate([-12/2, -12.5/2, -halfZ])
            cylinder(h=cornerH1*2, d=1, $fn=32, center=true);

            translate([-12/2, 12.5/2, -halfZ])
            cylinder(h=cornerH1*2, d=1, $fn=32, center=true);

            translate([12/2, -10.5/2, -halfZ])
            cylinder(h=cornerH1*2, d=1, $fn=32, center=true);

            translate([12/2, 10.5/2, -halfZ])
            cylinder(h=cornerH1*2, d=1, $fn=32, center=true);
        }
        
        CornerCutout(cornerH2);
        mirror([1,0,0])
        CornerCutout(cornerH2);
        mirror([0,1,0])
        {
            CornerCutout(cornerH2);
            mirror([1,0,0])
            CornerCutout(cornerH2);
        }

        translate([l/2,0,-h/2])
        cube([2, 100, cornerH2*2], center=true);

        translate([-l/2,0,-h/2])
        cube([2, 100, cornerH2*2], center=true);

        //Slanted sides
        translate([0, w/2 + 1.5, 1])
        rotate([30, 0, 0])
        cube([l*2, 3, h*2], center=true);

        mirror([0,1,0])
        translate([0, w/2 + 1.5, 1])
        rotate([30, 0, 0])
        cube([l*2, 3, h*2], center=true);

        //Pin side
        translate([l/2 + 2.5, 0, 0])
        rotate([0, -30, 0])
        cube([3, w*2, h*2], center=true);

        mirror([1,0,0])
        translate([l/2 + 1.5, 0, 0])
        rotate([0, -30, 0])
        cube([4, w*2, h*2], center=true);
    }
    
}

module CornerCutout(h2)
{
    a = 1.1;
    b = 1.8;
    x = l/2;
    y = w/2;
    z = -h/2;
    translate([x,y,z])
    cube([a*2, b*2, h2*2], center=true);
}

module SwitchKey(square = true)
{
    union()
    {
        if(square)
        {
            difference()
            {
                cube([6.50, 6.50, 4.00], center=true);

                translate([0, 0, 0.2])
                cylinder(d=6.30, h=3.80, center=true, $fn=64);
            }
        }
        

        cube([4.00, 1.10, 4.00], center=true);
        cube([1.10, 4.00, 4.00], center=true);
    }
}

module SwitchFull(pressed = 0, squareMount = true)
{
    color("#4DA1B2")
    SwitchBody();

    color("#CC3333")
    translate([0,0, halfZ + 4.00/2 - pressed])
    SwitchKey(squareMount);
}

SwitchFull();
