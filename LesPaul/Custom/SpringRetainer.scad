use <./../complete_strum.scad>
use <UB/ub.scad>


d1 = 5.1;
d2 = 4 + (2*1);
d3 = 6;
d4 = 4 + 0.15;
d5 = 1.48;
d6 = 3;

offset1 = 25;
h1 = 5;
h2 = 25 + d1/2 + 1;
h3 = 7;

fn = 128;

module PCBPart()
{
    difference()
    {
        cylinder(h=h1, d=d1, center=true, $fn=fn);
        cylinder(h=h1 + 0.5, d=d6, center=true, $fn=fn);
    }
    
}

module PinPart(h4 = 10)
{   
    rotate([0,90,0])
    difference()
    {
        union(){
            cube([d2,d2,h2], center=true);
            //cylinder(h=h2, d=d2, center=true, $fn=fn);

            translate([-h3/2,0, h2/2 - (d1/2) - 1])
            rotate([0,90,0])
            cylinder(h=h3, d=d1, center=true, $fn=fn);

            translate([h4/2, 0, -h2/2 + 5])
            rotate([0,90,0])
            union()
            {
                difference()
                {
                    cylinder(h=h4, d=d3, center=true, $fn=fn);
                    translate([0,0, h4/2])
                    cylinder(h=h4, d=d4 + 0.05, center=true, $fn=fn);
                }

                cylinder(h=h4 - 1, d=d5, center=true, $fn=fn);
            }
            
            
        }
        
        cylinder(h=h2 + 0.5, d=d4, center=true, $fn=fn);

        translate([d2/2,0,0])
        cube([d2, d2 + 0.5, d2*2], center=true);
        /*
        translate([d2/2,0,d2/2])
        rotate([90,0,0])
        cylinder(h=d2 + 0.5, d=d2, center=true, $fn=fn);

        translate([d2/2,0,-d2/2])
        rotate([90,0,0])
        cylinder(h=d2 + 0.5, d=d2, center=true, $fn=fn);
        */
    }
}

module PinPartCombined(h4 = 10)
{
    union()
    {
        translate([h2/2,0,2])
        PinPart(h4);

        translate([h2/-2,0,2])
        mirror([1,0,0])
        PinPart(h4);
    }
}

module AlignmentGuide()
{   
    difference()
    {
        union()
        {
            cube([25, 10, 2], true);
            translate([12.5 - 1.25, 0, -1])
            cube([2.5, 10, 4], true);

            translate([4, 0, -2])
            cylinder(h=2, d=4, center=true, $fn=fn);
        }

        translate([-7.5, 0, -2])
        cylinder(h=10, d=d6, center=true, $fn=fn);
    }
}

module SpringShowcase()
{
    translate([25,0,5])
    {
        z = 2.2;
        for (a =[0:5])
        {
            translate([0,0,z*a])
            Coil(od=6, id=5, pitch=z, grad=360);
        }

        rotate_extrude(convexity = 10, $fn = 100)
        translate([2.75, 0, 0])
        circle(d = 0.5, $fn = 100);

        translate([0, 0, 6*z])
        rotate_extrude(convexity = 10, $fn = 100)
        translate([2.75, 0, 0])
        circle(d = 0.5, $fn = 100);
    }
}

/*
translate([offset1,0,20 - 1.5 - h1/2])
PCBPart();

translate([-offset1,0,20 - 1.5 - h1/2])
PCBPart();
*/
/*
difference()
{
    PinPartCombined(10);

    translate([33.1,0,0])
    cube([50,d2 + 1,30], center=true);

    mirror([1,0,0])
    translate([33.1,0,0])
    cube([50,d2 + 1,30], center=true);
}
*/

//translate([85/2 - 10, 0, 1 + 20])
AlignmentGuide();

/*
CompleteStrumReference(use_strum = false, use_pcb=true, use_switch=true, use_mount1 = false, use_mount2 = false, use_body=false, use_pinGuide1 = false, use_pinGuide2 = false);
*/