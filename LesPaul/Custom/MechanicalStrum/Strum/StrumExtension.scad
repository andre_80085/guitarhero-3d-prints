include <Round-Anything/polyround.scad>
use <NopSCADlib/utils/round.scad>

bodyWidth = 22;
bodyThickness = 2.25;

module StrumExtension()
{
    rotate([90,0,0])
    {
        linear_extrude(height = 5, center = true, convexity = 10, twist = 0)
        difference()
        {
            union()
            {
                difference()
                {
                    translate([0, 0, 0])
                    square([bodyWidth, 5], center=true);

                    translate([0, 15, 0])
                    square([40, 30], center=true);

                    translate([bodyWidth/2 + 2.5, 13.5, 0])
                    square([5, 30], center=true);

                    translate([bodyWidth/-2 - 2.5, 13.5, 0])
                    square([5, 30], center=true);
                }

                translate([0,2,0])
                square([bodyWidth - (bodyThickness*2), 4], center=true);

                translate([0,5,0])
                circle(d=10, $fn=48);

                translate([0,-1.5,0])
                square([bodyWidth + 11.25, 2.5], center=true);

                /*
                translate([-23.925,-4.4,0])
                rotate([0,0, 10])
                translate([7,0,0])
                square([7, 2], center=false);
                */
            }

            translate([0,5.75,0])
            circle(d=4.05, $fn=48);

            translate([-15,-3.25, 0])
            rotate([0,0, -20])
            square([12, 4], center=true);

            mirror([1,0,0])
            {
                translate([-15,-3.25, 0])
                rotate([0,0, -20])
                square([12, 4], center=true);
            }
        }
    }
}

module StrumSpringMount(length = 6, d = 4.95)
{
    union()
    {
        //z = length/2 - 1;
        z = length/2 - 2;

        translate([0,0,-z])
        cylinder(d=d, h=length, center=true, $fn=100);

        translate([0,0, -z - length/2])
        sphere(d=d, $fn=100);

        /*
        translate([0,0,0])
        rotate([90,0,0])
        cylinder(d=0.5, h=(d + 0.4), center=true, $fn=50);
        */
        rotate([90,0,0])
        linear_extrude(height = 7.5, center = true, convexity = 10, twist = 0)
        difference()
        {
            union()
            {
                //translate([0,2,0])
                //square([bodyWidth - ((bodyThickness + 0.05) * 2), 4], center=true);

                //translate([0,4,0])
                //square([13, 4], center=true);

                difference()
                {
                    translate([0,5,0])
                    circle(d=10, $fn=100);

                    translate([0,0,0])
                    square([bodyWidth - 12, 4], center=true);
                }   

                x = (bodyWidth - ((bodyThickness + 0.05) * 2)) / 2;

                points = [
                    [-3.5, 2],
                    [-5, 0],
                    [-x, 0],
                    [-x, 4],
                    [-3, 8],

                    [3, 8],
                    [x, 4],
                    [x, 0],
                    [5, 0],
                    [3.5, 2]
                ];

                polygon(points,$fn=100);
            }

            /*
            translate([0,-1,0])
            square([bodyWidth - 12, 4], center=true);
            */

            //translate([0,0,0])
            //square([bodyWidth - 12, 4], center=true);

            translate([0,5.75,0])
            circle(d=4.15, $fn=100);
        }
    }

}

module SpringHolder(outer = 4.95, length = 6)
{
    h1 = 4;
    difference()
    {
        union()
        {
            cylinder(d=9, h=h1, center=true, $fn=6);

            translate([0,0,(length/2) + h1/2])
            cylinder(h=length, d=outer, center=true, $fn=100);

            translate([5, 0, -1])
            cube([10,7.7945, 2], center=true);
        }

        translate([7,0,(length + 4)/2 - (h1/2)])
        cylinder(h=(length + 6), d=3, center=true, $fn=100);
    }
}

module StrumSpacer(length = 6)
{
    difference()
    {
        cylinder(h=length, d=10, center=true, $fn=100);
        cylinder(h=length + 1, d=4.15, center=true, $fn=100);
        
        translate([0,0,length/2])
        cylinder(h=length/2, d=8, center=true, $fn=100);
    }
}

module StrumFeets(springHolder = false)
{
    x = (bodyWidth - ((bodyThickness + 0.05) * 2)) / 2;
    x2 = 19;
    x3 = x + bodyThickness + 0.065;
    y = 5;

    points = [
        [0, y, 0],
        [x2 - 5, y, 0],
        [x2, 1, 0],
        [x2, 0, 0],
        [x2, -2, 1],
        //[x2 - 4, -2, 2],
        [x2 - 4, -2, 0],

        //[x3 + 5, 0, 0],
        [x3 + 2, -2, 0],
        [x3, -2, 0],
        [x3, 0, 0],

        [x, 0, 0],
        [x, -3.5, 0.75],
        [0, -10, 0],
        [0, 0, 0]
    ];

    points2 = [
        [0,y + 0.1,0],
        [20,y + 1,0],
        [20, y - 2, 0],
        [8, y - 2, 0],
        [3.5, -2,0],
        [0, -2,0]
    ];

    points3 = [
        [0,     6,  0],
        [2.15,     6,  1],
        [2.35,     3,  0],
        [2.35,   0, 0],
        [0,   0, 0]
    ];



    union()
    {
        rotate([-90,0,0])
        difference()
        {
            linear_extrude(height = 10, center = true, convexity = 10, twist = 0)
            difference()
            {
                union()
                {
                    polygon(polyRound(points,100));
                    mirror([1,0,0])
                    polygon(polyRound(points,100));

                    translate([0,-5,0])
                    circle(d=10, $fn=100);
                }

                translate([0,-5.75,0])
                circle(d=4.15, $fn=100);
            }

            //spring holder cutout
            if(springHolder)
            {
                linear_extrude(height = 7, center = true, convexity = 10, twist = 0)
                {
                    polygon(polyRound(points2,100));
                    mirror([1,0,0])
                    polygon(polyRound(points2,100));
                }
            }
        }
        

        if(springHolder)
        {
            //translate([0,0,(3 + 4)/2 - (4/2)])
            //cylinder(h=(4), d=3, center=true, $fn=100);

            translate([0, 0, 2])
            rotate([0,180,0])
            rotate_extrude(convexity = 10, $fn = 360)
            polygon(polyRound(points3,100));
        }
    }
    
    
}

module StrumExtension2()
{
    x = (bodyWidth - ((bodyThickness + 0.05) * 2)) / 2;
    x2 = x + bodyThickness + 1;
    y = 1.5;

    points = [
        [0, y, 0],
        [x2, y, 0.5],
        [x2, 0, 0],

        [x, 0, 0],
        [x, -3.5, 0.75],
        [0, -10, 0],
        [0, 0, 0]
    ];

    union()
    {
        rotate([-90,0,0])
        linear_extrude(height = 5, center = true, convexity = 10, twist = 0)
        difference()
        {
            union()
            {
                polygon(polyRound(points,100));
                mirror([1,0,0])
                polygon(polyRound(points,100));

                translate([0,-5,0])
                circle(d=10, $fn=100);
            }

            translate([0,-5.75,0])
            circle(d=4.15, $fn=100);
        }
    }
    
    
}

module SwitchMount3(spacing = 1)
{
    //w = 17 * 2 + spacing;
    w = 54;
    offset = 7 + spacing/2;
    offset2 = offset + 12;

    color("#FF9100")
    union()
    {
        linear_extrude(height = 1.41, center = true, convexity = 10, twist = 0)
        difference()
        {
            round(1)
            square([20, w], center=true);
            
            translate([0,offset,0])
            square([13.95, 13.95], center=true);

            translate([0,-offset,0])
            square([13.95, 13.95], center=true);

            translate([0,offset2, 0])
            circle(d=3.1, $fn=100);

            translate([0,-offset2, 0])
            circle(d=3.1, $fn=100);
        }

        translate([0,0, -1.41/2 - 0.5])
        linear_extrude(height = 1, center = true, convexity = 10, twist = 0)
        {
            difference()
            {
                round(1)
                square([20, w], center=true);

                round(1)
                square([20 - 2, w - 2], center=true);
            }

            square([20, 1], center=true);
        }

        /*
        translate([0,0, -1.95/2 - (0.75/2)])
        linear_extrude(height = 0.75, center = true, convexity = 10, twist = 0)
        {
            translate([8.5, 0, 0])
            square([3, w], center=true);

            translate([-8.5, 0, 0])
            square([3, w], center=true);

            difference()
            {
                translate([0,offset2,0])
                circle(d=6, $fn=100);

                translate([0,offset2,0])
                circle(d=3.1, $fn=100);
            }

            difference()
            {
                translate([0,-offset2,0])
                circle(d=6, $fn=100);

                translate([0,-offset2,0])
                circle(d=3.1, $fn=100);
            }
        }
        */
    }
    
}

d_print = 4.75;

//SpringHolder(outer = d_print, length=7);
//StrumExtension();
//StrumSpringMount(length = 3, d=d_print);
//StrumSpacer(length = 3.5);

//StrumFeets(true);
//StrumExtension2();

SwitchMount3(4.75);