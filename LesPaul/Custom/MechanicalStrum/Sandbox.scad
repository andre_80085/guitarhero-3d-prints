use <./Strum/Strum.scad>
use <./Strum/StrumpinGuide.scad>
use <./../../strum_bodymount.scad>
use <NopSCADlib/core.scad>


use <NopSCADlib/utils/round.scad>
use <NopSCADlib/utils/layout.scad>
include <NopSCADlib/vitamins/springs.scad>

use <./strum_pcb_mount.scad>
use <./../../../util/RoundedTube.scad>
use <./../../../util/MxSwitch.scad>
use <./LinearGear.scad>
use <./SwitchMount.scad>
use <./SliderPart.scad>

showBolts = true;
showBodyMounts = false;
showBody = false;

railsZ = 3;
rail_length = 48;
d = 6;
sliderW = 14;
endPartX = rail_length/2 + 1.5;

display_switch = true;

StrumPCBMountOffetZ = 4;
PinOffsetZ = 2;
StrumOffetZ = 7.75;


color("#707070")
translate([0,0, PinOffsetZ])
rotate([0, 90, 0])
cylinder(d=4, h=85, center=true, $fn=32);

pinOffsetX = 29.75;
color("#303030")
translate([pinOffsetX, 0, PinOffsetZ])
rotate([0, 90, 0])
PinGuide(6.5, 9, 4, 0.5, 64);

color("#303030")
translate([-pinOffsetX, 0, PinOffsetZ])
rotate([0, 90, 180])
PinGuide(6.5, 9, 4, 0.5, 64);


if(showBody)
{
    translate([0,0,0])
    rotate([0, 0, 90])
    StrumBodyMount();
}

if(showBodyMounts)
{
    translate([-37,0,4])
    StrumPCBMountPart();

    mirror([1,0,0])
    translate([-37,0,4])
    StrumPCBMountPart();
}

translate([0,0, StrumOffetZ])
rotate([0, 180, 90])
{
    color("#AAAA00")
    StrumBody();

    color("#4AE51A")
    StrumGear();
}

module SliderRod(h = 40, d=4, show_springs = true, spring_l = 15)
{
    holeZ = h/2 - 1.25;

    rotate([90,0,0])
    {
        color("#CC6933")
        cylinder(h=h, d=d, center=true, $fn=48);

        if(show_springs)
        {
            translate([0,0, 6])
            comp_spring(["peg_spring",  9.5,  0.7,  spring_l,  7, 1, false, 0, "silver"], l=(spring_l - 1));

            mirror([0,0,1])
            translate([0,0, 6])
            comp_spring(["peg_spring",  9.5,  0.7,  spring_l,  7, 1, false, 0, "silver"], l=(spring_l - 1));
        }
        
    }
}

translate([0,0,16])
{
    LinearGear(teeths = 4);

    translate([0, 0, railsZ])
    rotate([90,0,0])
    SliderCenterPart(d = d, w = sliderW);
    /*
    translate([sliderW,0,railsZ])
    SliderRod(h = 56, d = d, spring_l = 15, show_springs=false);
    
    mirror([1,0,0])
    translate([sliderW,0,railsZ])
    SliderRod(h = 56, d = d, spring_l = 15);
    */
    
    translate([0, endPartX, railsZ - 0.25])
    SwitchMount(width = sliderW, d=d, cutout=false);

    mirror([0,1,0])
    translate([0, endPartX, railsZ - 0.25])
    SwitchMount(width = sliderW, d=d);
    
    
    if(display_switch)
    {
        translate([0,22, 1.75])
        {
            rotate([90,0,0])
            color("#5733CC")
            SwitchFull();

            translate([0,6.25,0])
            color("#CC3333")
            cube([18.9, 1.5, 18.9], center=true);
        }

        mirror([0,1,0])
        translate([0,22, 1.75])
        {
            rotate([90,0,0])
            color("#5733CC")
            SwitchFull();

            translate([0,6.25,0])
            color("#CC3333")
            cube([18.9, 1.5, 18.9], center=true);
        }
    }
}